const projectService=require('../service/project');
const userService=require('../service/user');
const pmService=require('../service/projectMember');
const tstService=require('../service/tester2');
const bugService=require('../service/bug');
const rbService=require('../service/resolvedBug');

const editRBug=async(req,res,next)=>{
    const user=req.body.email;
    const project=req.body.projID;
    const bug=req.body.id;
    const sol=req.body.solutionLink;
    try{
        
        let uCheck=await userService.getByEmail(user);
        if(user){
            try{
                let pCheck=await projectService.getByID(project);
                if(pCheck){
                    try{
                        let pmCheck=await pmService.getPMFORPROJ(project,user);
                        if(pmCheck){
                            try{
                                //check if the bug exists
                                let bCheck=await bugService.getByBugID(bug);
                                if(bCheck){
                                    //check if the bug has been assigned to this user
                                    try{
                                        let rCheck=await rbService.checkIfBugIsAssignedToPM(bug,pmCheck.p_id);
                                        if(rCheck){
                                            //update solution 
                                            try{
                                                if(sol.trim()){
                                                    let updated=await rbService.editByRBugID(sol,rCheck.id);
                                                    res.status(200).json("Number of rows updated: "+updated);
                                                }else{
                                                    let updated=await rbService.editByRBugID(null,rCheck.id);
                                                    res.status(200).json("Number of rows updated: "+updated);
                                                }
                                            }catch(err){
                                                res.status(502).send({message:`${err}`});
                                            }
                                        }else{
                                            res.status(202).send({message:'User cannot add solution to this bug'});
                                        }
                                    }catch(err){
                                        res.status(502).send({message:`${err}`});
                                    }
                                }else{
                                    res.status(202).send({message:'Not project bug'});
                                }
                            }catch(err){
                                 res.status(502).send({message:`${err}`});
                            }
                        }else{
                            res.status(202).send({message:'Not project member'});
                        }
                    }catch(err){
                         res.status(502).send({message:`${err}`});
                    }
                }else{
                    res.status(202).send({message:'No such project'});
                }
            }catch(err){
                res.status(502).send({message:`${err}`});
            }
        }else{
            res.status(202).send({message:'No such user'});
        }
    }catch(err){
        res.status(502).send({message:`${err}`});
    }
}
const assignBug=async(req,res,next)=>{
    const user=req.body.email;
    const project=req.body.projID;
    const bug=req.body.id;
    try{
        let uCheck=await userService.getByEmail(user);
        if(uCheck){
            try{
                let pCheck=await projectService.getByID(project);
                if(pCheck){
                    try{
                        let pmCheck=await pmService.getPMFORPROJ(project,user);
                        if(pmCheck){
                            try{
                                //check if the bug exists
                                let bCheck=await bugService.getByBugID(bug);
                                if(bCheck){
                                    //check if the bug has already been assigned
                                    try{
                                        let rCheck=await rbService.checkIfBugIsAssigned(bug);
                                        if(rCheck){
                                            res.status(202).send({message:'Bug already assigned'});
                                        }else{
                                            //assign bug
                                            try{
                                                let obj={
                                                    projectMemberPId:pmCheck.p_id,
                                                    bugId:bCheck.id
                                                }
                                                let rB=await rbService.create(obj);
                                                res.status(200).json(rB);
                                            }catch(err){
                                                res.status(502).send({message:`${err}`});
                                            }
                                        }
                                    }catch(err){
                                        res.status(502).send({message:`${err}`});
                                    }
                                }else{
                                    res.status(202).json({message:'Not project bug'});
                                }
                            }catch(err){
                                 res.status(502).send({message:`${err}`});
                            }
                        }else{
                            res.status(202).json({message:'Not project member'});
                        }
                    }catch(err){
                         res.status(502).send({message:`${err}`});
                    }
                }else{
                    res.status(202).json({message:'No such project'});
                }
            }catch(err){
                res.status(502).send({message:`${err}`});
            }
        }else{
            res.status(202).json({message:'No such user'});
        }
    }catch(err){
        res.status(502).send({message:`${err}`});
    }
}

const deAssignBug=async(req,res,next)=>{
    const user=req.query.email;
    const project=req.query.projID;
    const bug=req.query.id;
    try{
        let uCheck=await userService.getByEmail(user);
        if(uCheck){
            try{
                let pCheck=await projectService.getByID(project);
                if(pCheck){
                    try{
                        let pmCheck=await pmService.getPMFORPROJ(project,user);
                        if(pmCheck){
                            try{
                                //check if the bug exists
                                let bCheck=await bugService.getByBugID(bug);
                                if(bCheck){
                                    //check if the bug has already been assigned
                                    try{
                                        let rCheck=await rbService.checkIfBugIsAssigned(bug);
                                        if(rCheck){
                                            //bug is assigned
                                            //check if bug is assigned to PM
                                            try{
                                                let assignCheck=await rbService.checkIfBugIsAssignedToPM(bug,pmCheck.p_id);
                                                if(assignCheck){
                                                     //check if solution exists
                                                     if(assignCheck.solution){
                                                         //cannot deassign bug
                                                         res.status(202).send({message:'Solution exists; cannot deassign bug'});
                                                     }else{
                                                         let deassign=await rbService.deleteRB(assignCheck.id);
                                                          res.status(200).json(deassign);
                                                     }
                                                }else{
                                                    res.status(202).send({message:'Bug is not assigned to this user'});
                                                }
                                            }catch(err){
                                                res.status(502).send({message:`${err}`});
                                            }
                                        }else{
                                           //bug is not assigned => cannot deassign
                                           res.status(202).send({message:'Bug is not assigned'});
                                        }
                                    }catch(err){
                                        res.status(502).send({message:`${err}`});
                                    }
                                }else{
                                    res.status(202).send({message:'Not project bug'});
                                }
                            }catch(err){
                                 res.status(502).send({message:`${err}`});
                            }
                        }else{
                            res.status(202).send({message:'Not project member'});
                        }
                    }catch(err){
                         res.status(502).send({message:`${err}`});
                    }
                }else{
                    res.status(202).send({message:'No such project'});
                }
            }catch(err){
                res.status(502).send({message:`${err}`});
            }
        }else{
            res.status(202).send({message:'No such user'});
        }
    }catch(err){
        res.status(502).send({message:`${err}`});
    }
}

module.exports={
    assignBug,editRBug,deAssignBug
}