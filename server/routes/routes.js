const express=require('express')
const router=express.Router();
//for user -> add one,get all, get one, edit one
const {createUser,getAllUsers,editByEmail,getByEmail,getEmailPassword,deleteUser}=require('../controller/user');
router.post('/register',createUser); //for regsitration WORKS
router.get('/allUsers',getAllUsers); //getting all users WORKS
router.get('/user/:bemail',getByEmail); //gettinga user WORKS
router.put('/user/edit',editByEmail); //editing user WORKS
router.get('/logIn',getEmailPassword); //for login WORKS
router.delete('/user/deleteU',deleteUser);//remove user -> bugs from tst remain; resolvedbugs from pm remain; all project with only the user as pm will be deleted WORKS

//for projects -> add one, edit one, get all, get one, delete one, get all projects for whch you are tester, get all projects for which you are member
const {createProject,getAllProjects,editByID,getByID,getByName,deleteProject}=require('../controller/project');
router.post('/user/addProject',createProject); //create a project + adds ProjectMembers WORKS
router.get('/projects',getAllProjects);//getting all projects WORKS 
router.put('/user/project/edit',editByID); //!!!!!!!editing a project -> check again 
router.get('/project',getByID); //!!!!!!!!!getting a project when we know only the id -> check again -> check if the bugs for which the testers have been deleted appear, check if the solutions for bugs appear if the PM has been deleted
router.get('/projectSearch',getByName); //getting a project when we know a part of the name WORKS
router.delete('/user/project/deleteP',deleteProject);//remove project WORKS

//for tester
const {createTester,getAllTesters,getByProjectId,getByTstId,getByUserId,getProjectForATst,getUserIDProjIDTST,deleteTester}=require('../controller/tester');
router.get('/user/:bemail/TSTprojects',getProjectForATst); //get all projects for which a user is a tester WORKS
router.post('/user/project/tester', createTester); //a user becomes a tester WORKS
router.get('/user/project/istester',getUserIDProjIDTST); //if a user is a tester for a specific project WORKS
router.delete('/user/:bemail/project/:bpid/deleteT',deleteTester);//remove yourself as tester from project-> all assigned bugs of that user will have tester id = null WORKS

//for PM
const {createPM,getAllPM,getByProjectIdPM,getByPMId,getByUserIdPM,getProjectForAPM,getProjectForPM,deletePM}=require('../controller/projectMember');
router.get('/user/:bemail/projects',getProjectForPM); //get all projects for which a user is a project member WORKS
router.get('/user/project/IsPM',getProjectForAPM);//if a user is a project member for a specific project WORKS
router.delete('/user/:bemail/project/:bid/deletePM',deletePM);//remove yourself as pm from project-> resolvedbugs will have pmid =null; if you are the only pm -> delete project WORKS


//for bug -> check again all of them 
const {createBug,editBug,getBugTSTPrj,getBugPrj,deleteBug}=require('../controller/bug');
router.post('/user/project/addBug',createBug); //add a bug WORKS
router.get('/user/project/bugs',getBugTSTPrj); //bugs for that user and that project WORKS
router.get('/project/bugs',getBugPrj); //all bugs for project check again -> if includes the busg that have no tester since it has been deleted
router.put('/user/project/bug/editBug',editBug); //update bug WORKS
router.delete('/user/project/bug/deleteBug',deleteBug); //delete bug only if it has not been assigned WORKS


//for resolved bugs
const {assignBug,editRBug,deAssignBug}=require('../controller/resolvedBug');
router.post('/user/project/bug/assign',assignBug); //assign a bug to a pm WORKS
router.put('/user/project/bug/addSolution',editRBug); //edit/add/delete the solution WORKS
router.delete('/user/project/bug/deassign',deAssignBug)//de-assign a bug to a pm if no sol is provided -> delete resolved bug; WORKS


module.exports=router;