const pmService=require('../service/projectMember');
const prjService=require('../service/project');
const userService=require('../service/user');

const createPM=async(req,res,next)=>{
    const userEmail=req.params.bemail;
    const prjID=req.params.bid;
    try{
        const result=await pmService.create(userEmail,prjID);
        res.status(200).json(result);
    }catch(err){
        res.status(500).send({message:'Error at createPM'});
    }
   
}

const getAllPM=async(req,res,next)=>{
    try{
        const results=await pmService.getAll();
        res.status(200).json(results);
    }catch(err){
         res.status(500).send({message:'Error at getAllPM'});
    }
}

const getByProjectIdPM=async(req,res,next)=>{
    try{
        const prjId=req.body.id;
        const userId=req.body.userId;
        const result=await pmService.getByPROJECTID(prjId);
        res.status(200).json(result);
    }catch(err){
         res.status(500).send({message:'Error at getByProjectIdPM'});
    }
}

const getByPMId=async(req,res,next)=>{
    try{
        const id=req.body.id;
        if(id){
            try{
                const result=await pmService.getByPMID(id);
                res.status(200).json(result);
            }catch(err){
                res.status(500).send({message:`Error:${err}`});
            }
        }else{
              res.status(2102).send({message:'Incomplete'});
        }
    }catch(err){
          res.status(500).send({message:`Error:${err}`});
    }
}
const getByUserIdPM=async(req,res,next)=>{
     try{
        const userId=req.params.bemail;
        const result=await pmService.getByUSERID(userId);
        res.status(200).json(result);
    }catch(err){
         res.status(500).send({message:'Error at getByUserIdPM'});
    }
}

const getProjectForAPM=async(req,res,next)=>{
        const id=req.query.email;
        const prjID=req.query.projID;
        try{
            //check if the project exists
            let pExists=await prjService.getByID(prjID);
            if(pExists){
                //check is user exists
                try{
                    let user=await userService.getByEmail(id);
                    if(user){
                        try{
                            const result=await pmService.getPMFORPROJ(prjID,id);
                            if(result){
                                res.status(200).json(result);
                            }
                            else{
                                res.status(210).json({message:'The user is not a project member for this project'});
                            }  
                        }catch(err){
                            res.status(500).send({message:`Error:${err}`});
                        }
                    }else{
                        res.status(210).json({message:'The user does not exist'});
                    }
                }catch(err){
                    res.status(500).send({message:`Error:${err}`});
                }
            }else{
                res.status(210).json({message:'The project does not exist'});
            }
        }catch(err){
            res.status(500).send({message:`Error:${err}`});
        }
    }

const getProjectForPM=async(req,res,next)=>{
     try{
        const userId=req.params.bemail;
        if(userId){
            try{
                const result=await pmService.getProjectForPM(userId);
                if(result)
                    res.status(200).json(result);
                else{
                    res.status(210).send({message:'No projects for this user'});
                }
            }catch(err){
                res.status(500).send({message:`Error:${err}`});
            }
        }
    }catch(err){
         res.status(500).send({message:`Error:${err}`});
    }
}
const deletePM=async(req,res,next)=>{
    const id=req.params.bemail;
    const prjID=req.params.bid;
        try{
            //check if the project exists
            let pExists=await prjService.getByID(prjID);
            if(pExists){
                //check is user exists
                try{
                    let user=await userService.getByEmail(id);
                    if(user){
                        try{
                            const result=await pmService.getPMFORPROJ(prjID,id);
                            if(result){
                                //the user is a pm
                                try{
                                    //check if user is the only PM -> if the user is the only PM -> delete whole project
                                    let pms=await pmService.getByPROJECTID(prjID);
                                    if(pms.length>1){
                                        try{
                                        let del=await pmService.deletePMember(prjID,id);
                                        res.status(200).json(del);
                                        }catch(err){
                                            res.status(500).send({message:`Error:${err}`});
                                        }
                                    }else{
                                        //the user is the only pm -> delete the whole project
                                        try{
                                            let del=await prjService.deletePJ(prjID);
                                            res.status(200).send({message:'Deleted project'});
                                        }catch(err){
                                            res.status(500).send({message:`Error:${err}`});
                                        }
                                    }
                                }catch(err){
                                     res.status(500).send({message:`Error:${err}`});
                                }
                            }
                            else{
                                res.status(210).send({message:'The user is not a project member for this project'});
                            }  
                        }catch(err){
                            res.status(500).send({message:`Error:${err}`});
                        }
                    }else{
                        res.status(210).send({message:'The user does not exist'});
                    }
                }catch(err){
                    res.status(500).send({message:`Error:${err}`});
                }
            }else{
                res.status(210).send({message:'The project does not exist'});
            }
        }catch(err){
            res.status(500).send({message:`Error:${err}`});
        }
}

module.exports={
    createPM,
    getAllPM,
    getByProjectIdPM,
    getByPMId,
    getByUserIdPM,
    getProjectForAPM,
    getProjectForPM,
    deletePM
}