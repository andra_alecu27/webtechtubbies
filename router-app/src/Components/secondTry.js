import React, { Component } from 'react';
const axios=require('axios');
//3 types -> user, pm, tst
class HomePage extends Component{
    constructor(props){
        super(props);
        this.state={email:this.props.email,view:1,projID:null,msg:'',pwd:this.props.pwd};
    }
    exit=()=>{
        console.log(`In HomePage -> exit ${this.state.view}`);
        this.props.onChange();
    }
    goHome=()=>{
        console.log(`In HomePage -> goHome ${this.state.view}`);
        this.setState({view:1});
    }
    changehomeView=(value1,value2)=>{
        console.log(`In HomePage ->value1 ${value1}`);
        console.log(`In HomePage ->view before ${this.state.view}`);
        this.setState({view:value1,projID:value2},()=>console.log(`In HomePage -> view after ${this.state.view}`));
    }
    handleChangeID=(event)=>{
        this.setState({projID:event.target.value})
    }
    changeMessage=(value)=>{
        this.setState({msg:value});
    }
    goID=()=>{
        let str='http://3.16.216.202:8080/api/project';
        console.log(3);
        if(this.state.projID!==null)
        axios.get(str,{params:{projID:this.state.projID}}).then(res=>{
            console.log('in axios');
            if(res.data){
                if(res.data.message){
                    this.changeMessage(res.data.message);
                }else{ console.log(3);this.changehomeView(3,res.data.project.id);}
            }
        }).catch(err=>{console.log(err)});
        else this.changeMessage("Must write id");
    }
    render(){
        if(this.state.view===1){
            console.log("view=1 home")
            return(
            <div>
            <ul>
            <li><button type="submit" onClick={this.goHome} onChange={this.changehomeView}>Home</button></li>
            {/*<li><button type="submit" onClick={this.goPM}>Projects PM</button></li>
            <li><button type="submit" onClick={this.goTST}>Projects TST</button></li>
            <li><button type="submit" onClick={this.goName}>Search by name</button></li>
            <li><button type="submit" onClick={this.exit}>Log out</button></li>*/}
            <li><button type="submit" onClick={this.exit}>Log out</button></li>
            <li><input type="text" name="id" id="id" valus={this.state.projID} onChange={this.handleChangeID}/></li>
            <li><button type="submit" onClick={this.goID}>Search by ID</button></li>
            </ul>
            <View v={1} delete={this.exit} email={this.state.email} projID={this.state.projID} password={this.state.pwd} onChange={this.changehomeView}/>
            <div>{this.state.msg}</div>
            </div>)
        }else if(this.state.view===2){
            console.log("view=2 home")
            return(
            <div>
            <ul>
            <li><button type="submit" onClick={this.goHome} onChange={this.changehomeView}>Home</button></li>
            {/*<li><button type="submit" onClick={this.goPM}>Projects PM</button></li>
            <li><button type="submit" onClick={this.goTST}>Projects TST</button></li>
            <li><button type="submit" onClick={this.goName}>Search by name</button></li>
            <li><button type="submit" onClick={this.exit}>Log out</button></li>*/}
            <li><button type="submit" onClick={this.exit}>Log out</button></li>
            <li><input type="text" name="id" id="id" valus={this.state.projID} onChange={this.handleChangeID}/></li>
            <li><button type="submit" onClick={this.goID}>Search by ID</button></li>
            </ul>
            <View v={2} delete={this.exit} email={this.state.email} projID={this.state.projID} password={this.state.pwd} onChange={this.changehomeView}/>
            <div>{this.state.msg}</div>
            </div>)
        }else if(this.state.view===3){
            return(
            <div>
            <ul>
            <li><button type="submit" onClick={this.goHome} onChange={this.changehomeView}>Home</button></li>
            {/*<li><button type="submit" onClick={this.goPM}>Projects PM</button></li>
            <li><button type="submit" onClick={this.goTST}>Projects TST</button></li>
            <li><button type="submit" onClick={this.goName}>Search by name</button></li>
            <li><button type="submit" onClick={this.exit}>Log out</button></li>*/}
            <li><button type="submit" onClick={this.exit}>Log out</button></li>
            <li><input type="text" name="id" id="id" valus={this.state.projID} onChange={this.handleChangeID}/></li>
            <li><button type="submit" onClick={this.goID}>Search by ID</button></li>
            </ul>
            <View v={3} delete={this.exit} email={this.state.email} projID={this.state.projID} password={this.state.pwd} onChange={this.changehomeView}/>
            <div>{this.state.msg}</div>
            </div>)
        }else {
             return(
            <div>
            <ul>
            <li><button type="submit" onClick={this.goHome} onChange={this.changehomeView}>Home</button></li>
            {/*<li><button type="submit" onClick={this.goPM}>Projects PM</button></li>
            <li><button type="submit" onClick={this.goTST}>Projects TST</button></li>
            <li><button type="submit" onClick={this.goName}>Search by name</button></li>
            <li><button type="submit" onClick={this.exit}>Log out</button></li>*/}
            <li><button type="submit" onClick={this.exit}>Log out</button></li>
            <li><input type="text" name="id" id="id" valus={this.state.projID} onChange={this.handleChangeID}/></li>
            <li><button type="submit" onClick={this.goID}>Search by ID</button></li>
            </ul>
            <View v={4} delete={this.exit} email={this.state.email} projID={this.state.projID} password={this.state.pwd} onChange={this.changehomeView}/>
            <div>{this.state.msg}</div>
            </div>)
        }
    }
    
}

class View extends Component{
    constructor(props){
        super(props);
        this.state={email:this.props.email,v:this.props.v,projID:this.props.projID,password:this.props.password};
    }
    changeView=(value1,value2)=>{
        console.log(`In View ${value1}`);
        this.props.onChange(value1,value2);
    }
    exit=()=>{
        this.props.delete();
    }
    render(){
        if(this.props.v===1){
            console.log("v===1");
            return <Home exit={this.exit} password={this.state.pwd} email={this.state.email} onChange={this.changeView}/>;}
            else if(this.props.v===2){
                console.log("v===2");
                return <AddProject email={this.state.email} onChange={this.changeView}/>;}
                else if(this.props.v===3 && this.props.projID!==null){
                    console.log("v===3 "+ this.props.projID);
                    return <ViewProject email={this.state.email} projID={this.props.projID} onChange={this.changeView}/>;}
                    else if(this.props.v===4 && this.props.projID!==null)
                        return <EditProject email={this.state.email} projID={this.props.projID} onChange={this.changeView}/>
                        else return <Home password={this.state.pwd} email={this.state.email} onChange={this.changeView}/>;
    }
}

//in Home -> button to add project; button to edit password; button to delete account

export class Home extends Component{
    constructor(props){
        super(props);
        this.state={email:this.props.email,
        NewPassword:'',
        repeatNewPassword:'',
        msg: null}
    }
    changeMessage=(value)=>{
         this.setState({msg:value})
    }
    handleChangePWD=(event)=>{
        this.setState({NewPassword:event.target.value})
    }
    handleChangeRepeatPWD=(event)=>{
        this.setState({repeatNewPassword:event.target.value})
    }
    editUser=()=>{
        let u=this.state;
        console.log(u);
        let str='http://3.16.216.202:8080/api/user/edit';
        if(u.NewPassword===u.repeatNewPassword){
            axios.put(str,u).then(user=>{
                    console.log(user);
                    if(user.data){
                        if(user.data.message)
                            this.changeMessage(user.data.message);
                    else this.setState({NewPassword:'',repeatNewPassword:''});
                     }
            }).catch(err=>{console.log(err)});
        }else this.changeMessage("Passwords must match");
    }
    deleteUser=()=>{
         let str1=`http://3.16.216.202:8080/api/user/deleteU`;
        let u=this.state;
        axios.delete(str1,{params:{email:this.state.email}}).then((res)=>{
            if(res.data){
                if (res.data.message){
                    this.changeMessage(res.data.message);
                }else this.exit();
            }
        }).catch(err => {console.log(err)});
        
    }
    exit=()=>{
        this.props.exit();
    }
    
    changeView=()=>{
        console.log(`In Home`);
        this.props.onChange(2,null);
    }
    render(){
        return(
            <div>
                <div>{this.state.email}</div>
                <label htmlFor="NewPassword">New password</label>
                <input type="text" name="NewPassword" id="NewPassword" value={this.state.NewPassword} onChange={this.handleChangePWD}/>
                <label htmlFor="repeatNewPassword">Repeat Password</label>
                <input type="text" name="repeatNewPassword" id="repeatNewPassword" value={this.state.repeatNewPassword} onChange={this.handleChangeRepeatPWD}/>
                <button id="3" type="button" onClick={this.editUser}>Save changes</button>
                <p>{this.state.msg}</p>
                <br/>
                <button id="4" type="button" onClick={this.changeView}>Add project</button>
                <br/>
                <br/>
                <button id="5" type="button" onClick={this.deleteUser}>Delete account</button>
            </div>
        );
    }
}

class AddProject extends Component{
    constructor(props){
        super(props);
        this.state={email:this.props.email,
            msg: null,
            projectName:'',
            projectDescription:'',
            projectLink:'',
            projectMember1:'',
            projectMember2:'',
            projectMember3:'',
            projectMember4:'',
            projectMember5:'',
            projID:''
        }
    }
    changeView2=(value)=>{
        console.log(`In add project -> save -> else`);
        this.props.onChange(3,value);
    }
    changeMessage=(value)=>{
         this.setState({msg:value})}
    saveProject=()=>{
        console.log(`In addProject -> add project`);
        //axios -> add project
        let proj=this.state
        //console.log(proj);
        axios.post("http://3.16.216.202:8080/api/user/addProject",proj)/*{params:{email:proj},body:{proj}})*/.then(res=>{
            console.log(res);
            if(res.data){
                if(res.data.message){
                    this.changeMessage(res.data.message);
                }else{
                    console.log(`In addProject -> add project yes`);
                    console.log(res.data.project.id)
                    this.changeView2(res.data.project.id);
                }
            }
        }).catch(err=>{console.log(err)});;
    }
    handleChangeName=(event)=>{
        this.setState({projectName:event.target.value});
    }
    handleChangeDescription=(event)=>{
        this.setState({projectDescription:event.target.value});
    }
    handleChangeLink=(event)=>{
        this.setState({projectLink:event.target.value});
    }
    handleChangeProj1=(event)=>{
        this.setState({projectMember1:event.target.value});
    }
    handleChangeProj2=(event)=>{
        this.setState({projectMember2:event.target.value});
    }
    handleChangeProj3=(event)=>{
        this.setState({projectMember3:event.target.value});
    }
    handleChangeProj4=(event)=>{
        this.setState({projectMember4:event.target.value});
    }
    handleChangeProj5=(event)=>{
        this.setState({projectMember5:event.target.value});
    }
    changeView=()=>{
        console.log(`In addProject -> cancel`);
        this.props.onChange(1,null)
    }
    render(){
        return(
            <div>
                <h1>Add project</h1>
                <hr/>
                <label htmlFor="projectName">Project name</label>
                <input type="text" name="projectName" id="projectName" value={this.state.projectName} onChange={this.handleChangeName}/>
                
                <label htmlFor="projectDescription">Project description</label>
                <textarea id="projectDescription" type="text" name="projectDescription" rows="10" cols="50" value={this.state.projectDescription} onChange={this.handleChangeDescription}></textarea>
                
                <label htmlFor="projectLink">Project link</label>
                <input type="text" name="projectLink" id="projectLink" value={this.state.projectLink} onChange={this.handleChangeLink}/>
                <br/>
                <label htmlFor="projectMember">Project Members</label>
                <fieldset>
                  <legend>Project members:</legend>
                  PM2 <input type="text" name="projectMember1" value={this.state.projectMember1} onChange={this.handleChangeProj1}/><br/>
                  PM3 <input type="text" name="projectMember2" value={this.state.projectMember2} onChange={this.handleChangeProj2}/><br/>
                  PM4 <input type="text" name="projectMember3" value={this.state.projectMember3} onChange={this.handleChangeProj3}/><br/>
                  PM5 <input type="text" name="projectMember4" value={this.state.projectMember4} onChange={this.handleChangeProj4}/><br/>
                  PM6 <input type="text" name="projectMember5" value={this.state.projectMember5} onChange={this.handleChangeProj5}/>
                </fieldset>
                
                <button id="16" type="button" onClick={this.saveProject}>Add project</button>
                <p>{this.state.msg}</p>
                
                <br/>
                <button id="17" type="button" onClick={this.changeView}>Cancel</button>
                <br/>
            </div>
        );
    }
}


class ViewProject extends Component{
    constructor(props){
        super(props);
        this.state={email:this.props.email,projID:this.props.projID,msg: null, type:'user',noA:0,noB:0,noRB:0,project:null,change:0};
        console.log(this.state.type);
    }
    changeMessage=(value)=>{
         this.setState({msg:value})}
    becomeTester=()=>{
        let str=`http://3.16.216.202:8080/api/user/project/tester`;
        let state=this.state;
        axios.post(str,state).then(res=>{
            if(res.data){
                if(res.data.message){
                    this.changeMessage(res.data.message);
                }else this.setState({type:'tst'});
            }
        }).catch(err=>{console.log(err)});
    }
    onProjectChanged=()=>{
        let str1=`http://3.16.216.202:8080/api/project`;
        axios.get(str1,{params:{projID:this.state.projID}}).then(result=>{
            if(result.data){
                if(result.data.message){
                    this.setState({message:result.data.message})
                }else{
                    this.setState({project:result.data});
                }
            }
           
        })
    }
    // componentDidUpdate=(prevState)=>{
    //     if(prevState.change!==this.state.change){
    //          let str1=`http://3.16.216.202:8080/api/project`;
    //     let str2=`http://3.16.216.202:8080/api/user/project/istester`;
    //     let str3=`http://3.16.216.202:8080/api/user/project/IsPM`;
    //     let state=this.state;
    //     let noB=0;
    //      let noRB=0;
    //      let noA=0;
    // axios.get(str1,{params:{projID:this.state.projID}}).then(res=>{
    //     if(res.data.message){
    //         this.changeMessage(res.data.message)
    //     }else{
    //         let p=res.data;
    //         if(p.bgsNoT)
    //         noB=p.bgsNoT.length;
    //         if(p.bgsNoT)
    //         for(let i=0;i<p.bgsNoT.length;i++){
    //              if(p.bgsNoT[i].solutionLink)
    //                  noRB++;
    //              if(p.bgsNoT[i].solutionID)
    //                  noA++;
    //          }
    //     if(p.tsts)
    //         for(let i=0;i<p.tsts.length;i++){
    //             if(p.tsts[i].bugs){
    //              noB=noB+p.tsts[i].bugs.length;
    //              for(let j=0;j<p.tsts[i].bugs.length;j++){
    //                  if(p.tsts[i].bugs[j].solution[0].solutionLink){
    //                      noRB++;
    //                  }
    //                  if(p.tsts[i].bugs[j].solution[0].solutionID){
    //                      noA++;
    //                  }
    //              }
    //             }
    //          }
    //         axios.get(str2,{params:{projID:this.state.projID,email:this.props.email}}).then(result=>{
    //             if(result.data.message){
    //                 axios.get(str3,{params:{projID:this.state.projID,email:this.props.email}}).then(res=>{
    //                     if(res.data.message){ 
    //                         this.setState({noA:noA,noB:noB,noRB:noRB});
    //                     }else  this.setState({type:"pm",noA:noA,noB:noB,noRB:noRB,});})
    //                 }   
    //                 else this.setState({type:"tst",noA:noA,noB:noB,noRB:noRB});
    //         }) 
    //     }
    //     }).catch(err => {console.log(err)});
    //     }
    // }
     onBugAdded =() => {
         let str1=`http://3.16.216.202:8080/api/project`;
         console.log("in onBug added in view project")
            axios.get(str1,{params:{projID:this.state.projID}}).then(result=>{
            if(result.data){
                if(result.data.message){
                    this.setState({message:result.data.message})
                }else{
                    let c=this.state.change;
                    console.log('In onBugAdded');
                    console.log(c);
                    c++;
                    this.setState({project:result.data,change:c});
                }
            }
           
        })
       // this.setState({change:1});
     }
    componentDidMount=()=>{
        let str1=`http://3.16.216.202:8080/api/project`;
        let str2=`http://3.16.216.202:8080/api/user/project/istester`;
        let str3=`http://3.16.216.202:8080/api/user/project/IsPM`;
        let state=this.state;
        let noB=0;
         let noRB=0;
         let noA=0;
    axios.get(str1,{params:{projID:this.state.projID}}).then(res=>{
        if(res.data.message){
            this.changeMessage(res.data.message)
        }else{
            let p=res.data;
            if(p.bgsNoT)
            noB=p.bgsNoT.length;
            if(p.bgsNoT)
            for(let i=0;i<p.bgsNoT.length;i++){
                 if(p.bgsNoT[i].solutionLink)
                     noRB++;
                 if(p.bgsNoT[i].solutionID)
                     noA++;
             }
        if(p.tsts)
            for(let i=0;i<p.tsts.length;i++){
                if(p.tsts[i].bugs){
                 noB=noB+p.tsts[i].bugs.length;
                 for(let j=0;j<p.tsts[i].bugs.length;j++){
                     if(p.tsts[i].bugs[j].solution[0].solutionLink){
                         noRB++;
                     }
                     if(p.tsts[i].bugs[j].solution[0].solutionID){
                         noA++;
                     }
                 }
                }
             }
            axios.get(str2,{params:{projID:this.state.projID,email:this.props.email}}).then(result=>{
                if(result.data.message){
                    axios.get(str3,{params:{projID:this.state.projID,email:this.props.email}}).then(res=>{
                        if(res.data.message){ 
                            this.setState({noA:noA,noB:noB,noRB:noRB});
                        }else  this.setState({type:"pm",noA:noA,noB:noB,noRB:noRB,});})
                    }   
                    else this.setState({type:"tst",noA:noA,noB:noB,noRB:noRB});
            }) 
        }
        }).catch(err => {console.log(err)});
    }
    changeViewH=()=>{
        this.props.onChange(1,null)
    }
     changeViewE=()=>{
        this.props.onChange(4,this.state.projID);
    }
    deleteProject=()=>{
        let str1=`http://3.16.216.202:8080/api/user/project/deleteP`;
        let u=this.state;
        axios.delete(str1,{params:{email:this.state.email,projID:this.state.projID}}).then((res)=>{
            if(res.data){
                if (res.data.message){
                    this.changeMessage(res.data.message);
                }else this.changeViewH();
            }
        }).catch(err => {console.log(err)});
    }
    render(){
        if(this.state.type==="pm"){
            console.log('is pm');
            return(
                <div>
                <button id="8" type="button" onClick={this.changeViewH}>Go back</button>
                <ProjectDetails project={this.state.project} />
                <button id="19" type="button" onClick={this.changeViewE}>Edit project</button>
                <button id="100" type="button" onClick={this.deleteProject}>Delete project</button>
                <BugList onProjectChanged={this.state.onProjectChanged} email={this.state.email} projID={this.state.projID} change={this.state.change}/>
                </div>
            );
        }else if(this.state.type==="user"){
            console.log('is user');
            return(
                <div>
                <button id="8" type="button" onClick={this.changeViewH}>Go back</button>
                <ProjectDetails projID={this.state.projID}/>
                <button id="90" type="button" onClick={this.becomeTester}>Become a tester</button>
                <BugList onProjectChanged={this.state.onProjectChanged} email={this.state.email} projID={this.state.projID} change={this.state.change}/>
                </div>
            );
        }else {
            console.log('is tst');
            return(
                <div>
                <button id="8" type="button" onClick={this.changeViewH}>Go back</button>
                <ProjectDetails projID={this.state.projID}/>
                <AddBug change={this.state.change} onBugAdded={this.onBugAdded} email={this.state.email} projID={this.state.projID}/>
                <BugList onProjectChanged={this.state.onProjectChanged} email={this.state.email} projID={this.state.projID} change={this.state.change}/>
                </div>
            );
        }
    }
    
}

class ProjectDetails extends Component{
    constructor(props){
        super(props);
        this.state={project:this.props.project,msg: null,id:this.props.projID,name:'',desc:'',link:'',pjm:[]}
    }
    changeMessage=(value)=>{
         this.setState({msg:value})}
    componentDidMount=()=>{
            //let state=this.state;
            let str='http://3.16.216.202:8080/api/project';
            axios.get(str,{params:{projID:this.state.id}}).then((res)=>{
                if(res.data){
                    if(res.data.message){
                        this.changeMessage(res.data.message);
                        //return -1;
                    }else{
                        this.setState({name:res.data.project.name,desc:res.data.project.description,link:res.data.project.link,pjm:res.data.pm});
                        }
                    }
                }).catch(err => {console.log(err)});
    }
    render(){
        const projectMembers = this.state.pjm.map((projectM,index) => <div key={index}>{projectM.p_id} : {projectM.userEmail}</div>)
        return(
            <div>
                <h1>{this.state.id} {this.state.name}</h1>
                <p>{this.state.desc}</p>
                <h4>{this.state.link}</h4>
                <div>
                {projectMembers}
                </div>
            </div>
        )
        
    }
}

class AddBug extends Component{
    constructor(props){
        super(props);
        this.state={email:this.props.email,projID:this.props.projID,msg: null,desc:'',severity:'',link:''}
    }
    changeMessage=(value)=>{
         this.setState({msg:value})}
    addBug=()=>{
        let b=this.state;
        let str=`http://3.16.216.202:8080/api/user/project/addBug`;
        console.log('in add bug');
        //console.log(b);
        axios.post(str,b).then(res=>{
            if(res.data){
                console.log(res);
                if(res.data.message){
                    this.changeMessage(res.data.message);
                }else{
                    console.log(res.data);
                   //this.setState({desc:'',severity:'',link:''});
                   //this.setState({change:1});
                   this.bugAdded();
                }
            }
        }).catch(err=>{console.log(err)});
    }
    bugAdded=()=>{
        this.props.onBugAdded();
    }
    handleChangeDescription=(event)=>{
        this.setState({desc:event.target.value});
    }
    handleChangeLink=(event)=>{
        this.setState({link:event.target.value});
    }
    handleChangeSeverity=(event)=>{
        this.setState({severity:event.target.value});
    }
    render(){
        return(
            <div>
                <label htmlFor="desc">Description</label>
                <textarea  id="desc" type="text" name="desc" rows="10" cols="50" value={this.state.desc} onChange={this.handleChangeDescription}></textarea>
                <label htmlFor="link">Link</label>
                <input type="text" name="link" id="link" value={this.state.link} onChange={this.handleChangeLink}/>
                <label htmlFor="severity">Severity</label>
                <select 
                    value={this.state.severity}
                    onChange={this.handleChangeSeverity}>
                    <option>Critical</option>
                    <option>Major</option>
                    <option>Minor</option>
                    <option>Trivial</option>
                </select>
                <button type="submit" onClick={this.addBug}>Add bug</button>
                <p>{this.state.msg}</p>
            </div>
        );
    }
}
class BugList extends Component{
     constructor(props){
        super(props);
        this.state={email:this.props.email,projID:this.props.projID,msg:null,testers:null,bugsWithNoTester:null,change:this.props.change};
    }
    changeMessage=(value)=>{
        this.setState({msg:value})}
    getBugsForProjForTST=(email)=>{
        console.log(this.state.projID);
        // let str=`http://3.16.216.202:8080/api/user/project/bugs`;
        // let bugs=[];
        // axios.get(str,{params:{projID:this.props.projID,email:this.props.email}}).then((res)=>{
        //     if(res.data){
        //         if(res.data.message){
        //             this.changeMessage(res.data.message);
        //         }else{
        //              for(let i=0;i<res.data.tsts.length;i++){
        //                       bugs.push(res.data.tsts[i]);
        //                 }
        //               // for(let i=0;i<res.data.bgsNoT.length;i++)
        //             this.setState({bugs:bugs});
        //         }
        //     }
        // }).catch(err => {console.log(err)});
        for(let i=0;i<this.state.testers.length;i++){
            if(email===this.state.testers[i].tester.email){
                return this.state.testers[i].bugs;
            }
        }
    }
    componentDidUpdate=(prevProps)=>{
        if(prevProps.change!==this.props.change){
            console.log('changing');
            let state=this.state;
            let str='http://3.16.216.202:8080/api/project';
            let testersP=null;
            let bugs=[];
        axios.get(str,{params:{projID:this.state.projID}}).then((result)=>{
                if(result.data){
                    if(result.data.message){
                        this.changeMessage(result.data.message);
                        //return -1;
                    }else{
                        //return res.data.tsts;
                        axios.get(str,{params:{projID:this.state.projID}}).then((res)=>{
                            if(res.data){
                                if(res.data.message){
                                    this.changeMessage(res.data.message);
                                   
                                    //return -1;
                                }else{
                                     console.log('in updated');
                                     console.log(res);
                                    //this.setState({bugs:bugs});
                                    this.setState({testers:res.data.tsts,bugsWithNoTester:res.data.bgsNoT})
                                    }
                                }
                            }).catch(err => {console.log(err)});
                        }
                    }
                }).catch(err => {console.log(err)});
    }
    }
    
    componentDidMount=()=>{
        let state=this.state;
            let str='http://3.16.216.202:8080/api/project';
            let testersP=null;
            let bugs=[];
        axios.get(str,{params:{projID:this.state.projID}}).then((result)=>{
                if(result.data){
                    if(result.data.message){
                        this.changeMessage(result.data.message);
                        //return -1;
                    }else{
                        //return res.data.tsts;
                        axios.get(str,{params:{projID:this.state.projID}}).then((res)=>{
                            if(res.data){
                                if(res.data.message){
                                    this.changeMessage(res.data.message);
                                    //return -1;
                                }else{
                                    
                                    //this.setState({bugs:bugs});
                                    this.setState({testers:result.data.tsts,bugsWithNoTester:res.data.bgsNoT})
                                    }
                                }
                            }).catch(err => {console.log(err)});
                        }
                    }
                }).catch(err => {console.log(err)});
    }
    render(){
        let testerList=null;
        if(this.state.testers!==null && this.state.testers.length>0){
            let initialTesters=this.state.testers.filter((currentTester)=>{
                 let bgs=this.getBugsForProjForTST(currentTester.tester.email);
                 if(bgs){
                        return bgs.length>0
                 }else return false;
            })
            testerList = initialTesters.map((tst,index,array) => {
                let bgs=[];
                for(let i=0;i<array.length;i++){
                    if(array[i].tester.email===tst.email){
                        bgs.push(array[i].bugs)
                    }
                }
                return <TesterBugs key={index} change={this.state.change} onProjectChanged={this.props.onProjectChanged} email={this.state.email} projID={this.state.projID} tester={tst.email}/>})}
        return(
            <div>
            <div>
            {testerList}
            </div>
            <hr/>
            <BugsWithNoTester change={this.state.change} onProjectChanged={this.props.onProjectChanged} email={this.state.email} projID={this.state.projID} />
            </div>
        );
    }
}


class TesterBugs extends Component{
        constructor(props){
            super(props);
            this.state={email:this.props.email,projID:this.props.projID,msg: null,tester:this.props.tester,bugs:null,change:this.props.change};
        }
        // onChangeBug=()=>{
        //     this.setState({msg:null});
        // }
        componentDidUpdate=(prevProps)=>{
            if(prevProps.change!==this.props.change){
                let state=this.state;
            let str='http://3.16.216.202:8080/api/project';
            let currBugs=null;
            axios.get(str,{params:{projID:this.state.projID}}).then((res)=>{
                if(res.data){
                    if(res.data.message){
                        this.changeMessage(res.data.message);
                        //return -1;
                    }else{
                        for(let i=0;i<res.data.tsts.length;i++){
                            if(res.data.tsts[i].email===this.state.tester){
                               currBugs=res.data.tsts[i].bugs;
                               break;
                            }
                        }
                
                        if(currBugs.length>0)
                            this.setState({bugs:currBugs});
                        else this.setState({bugs:null});
                    }
                }
            }).catch(err => {console.log(err)});
            }
        }
        componentDidMount=()=>{
            let state=this.state;
            let str='http://3.16.216.202:8080/api/project';
            let currBugs=null;
            axios.get(str,{params:{projID:this.state.projID}}).then((res)=>{
                if(res.data){
                    if(res.data.message){
                        this.changeMessage(res.data.message);
                        //return -1;
                    }else{
                        for(let i=0;i<res.data.tsts.length;i++){
                            if(res.data.tsts[i].email===this.state.tester){
                                console.log('in if in testers bugs');
                               currBugs=res.data.tsts[i].bugs;
                               break;
                            }
                        }
                        // let t=this.state.tester;
                        // console.log('in testers bugs');
                        // console.log(this.state.tester);
                        console.log(currBugs);
                        if(currBugs.length>0)
                            this.setState({bugs:currBugs});
                        else this.setState({bugs:null});
                    }
                }
            }).catch(err => {console.log(err)});
        }
        render(){
            //console.log('in render');
            // console.log(this.props.tester);
            // console.log(this.state.tester);
            // console.log(this.props.email);
                let bgs=null;
                if(this.state.bugs!==null){
                    bgs= this.state.bugs.map((bug,index) => {
                        console.log(bug);
                        return <Bug key={index} change={this.state.change} onProjectChanged={this.props.onProjectChanged} id={bug.bug.id} projID={this.state.projID} email={this.state.email} />})
                return(
                    <div>
                    <h1>Tester {this.state.bugs[0].bug.testerTId}</h1>
                    <div>
                    {bgs}
                    </div>
                    </div>
                )
                }else return null;
        }
    }

class BugsWithNoTester extends Component{
    constructor(props){
        super(props);
        //this.state={noA:this.props.noA,noS:this.props.noS,noB:this.props.noB,projID:this.props.projID,email:this.props.email,bugs:this.props.bugs,type:this.props.type,msg:null};
        this.state={bugs:null,msg:'',projID:this.props.projID,email:this.props.email,change:this.props.change};   
    }
    onChangeBug=()=>{
        this.setState({msg:''});
    }
    componentDidUpdate=(prevProps)=>{
        if(prevProps.change!==this.props.change){
            let state=this.state;
        let str='http://3.16.216.202:8080/api/project';
        let currBugs=null;
        axios.get(str,{params:{projID:this.state.projID}}).then((res)=>{
            if(res.data){
                if(res.data.message){
                    this.changeMessage(res.data.message);
                    //return -1;
                }else{
                    this.setState({bugs:res.data.bgsNoT});
                }
            }
        }).catch(err => {console.log(err)});
        }
    }
    componentDidMount=()=>{
        let state=this.state;
        let str='http://3.16.216.202:8080/api/project';
        let currBugs=null;
        axios.get(str,{params:{projID:this.state.projID}}).then((res)=>{
            if(res.data){
                if(res.data.message){
                    this.changeMessage(res.data.message);
                    //return -1;
                }else{
                    this.setState({bugs:res.data.bgsNoT});
                }
            }
        }).catch(err => {console.log(err)});
    }
    render(){
        let bgs=null;
        if(this.state.bugs!==null && this.state.bugs.lenght>0) bgs = this.state.bugs.map((bug,index) => {return <Bug key={index} change={this.state.change} onProjectChanged={this.props.onProjectChanged} projID={this.state.projID} email={this.state.email} id={bug.id}/>})
        return(
                <div>
                {bgs}
                </div>
            );
    }
}

class Bug extends Component{
    constructor(props){
    //   let _isMounted = false;
        super(props);
        this.state={PM_EMAIL:'',change:this.props.change,solutionID:'',solutionLink:'',type:'',bug:null,msg:'',severity:'',link:'',desc:'',editB:false,id:this.props.id,email:this.props.email,projID:this.props.projID,assigned:false,isPMBug:false,isTSTBug:false};
    }
    componentDidUpdate=(prevProps)=>{
        if(prevProps.change!==this.props.change){
            let isAssigned=false;
        let isPMB=false;
        let isTstB=false;
        // if(this.state.bug.solution.solutionID)
        //     isAssigned=true;
        let state=this.state;
        let str='http://3.16.216.202:8080/api/project';
        axios.get(str,{params:{projID:this.state.projID}}).then((res)=>{
            if(res.data){
                if(res.data.message){
                    this.changeMessage(res.data.message);
                    //return -1;
                }else{
                    console.log(res);
                    //console.log()
                    let currBug=null;
                    console.log(this.props.id);
                    console.log(res.data.tsts[0].bugs[0].bug.id);
                    let found=false;
                    for(let i=0;i<res.data.tsts.length;i++){
                        
                        for(let j=0;j<res.data.tsts[i].bugs.length;j++){
                            
                            if(res.data.tsts[i].bugs[j].bug.id===this.props.id)
                                {
                                    console.log('found');
                                    currBug=res.data.tsts[i].bugs[j];
                                    found=true;
                                    break;
                                }}
                        if(found===true){
                            break;
                        }
                    }
                    for(let i=0;i<res.data.bgsNoT.length;i++)
                            if(res.data.bgsNoT[i].id===this.props.id)
                                {
                                    currBug=res.data.bgsNoT[i];
                                    break;
                                }
                    console.log('in bug view');
                    console.log(currBug);
                    if(currBug.solution[0].solutionID)
                                        isAssigned=true;
                   let check=false;
                                for(let i=0;i<res.data.pm.length;i++){
                                    if(res.data.pm[i].userEmail===this.state.email)
                                        check=true;
                                }
                                if(check===true){
                                    if(currBug.solution[0].PM_EMAIL===this.state.email)
                                    
                                        this.setState({type:"pm",PM_EMAIL:currBug.solution[0].PM_EMAIL,severity:currBug.bug.severity,desc:currBug.bug.description,link:currBug.bug.link,bug:currBug,assigned:isAssigned,isPMBug:true,isTSTBug:false});
                                        else this.setState({type:"pm",PM_EMAIL:currBug.solution[0].PM_EMAIL,severity:currBug.bug.severity,desc:currBug.bug.description,link:currBug.bug.link,bug:currBug,assigned:isAssigned,isPMBug:false,isTSTBug:false});
                                }else {
                                    let check2=false;
                                    for(let i=0;i<res.data.tsts.length;i++){
                                        if(res.data.tsts[i].tester.userEmail===this.state.email)
                                                check2=true;}
                                    if(check2===true){
                                        let isTBug=false;
                                            for(let i=0;i<res.data.tsts.length;i++){
                                                for(let j=0;j<res.data.tsts[i].bugs[j];j++)
                                                    if(res.data.tsts[i].bugs[j].id===this.state.id && res.data.tsts[i].tester.email===this.state.email)
                                                        {
                                                            isTBug=true;
                                                            break;
                                                        }
                                            }
                                        this.setState({type:"tst",PM_EMAIL:currBug.solution[0].PM_EMAIL,severity:currBug.bug.severity,desc:currBug.bug.description,link:currBug.bug.link,bug:currBug,assigned:isAssigned,isPMBug:false,isTSTBug:isTBug});
                                    }else{
                                        this.setState({type:"user",PM_EMAIL:currBug.solution[0].PM_EMAIL,severity:currBug.bug.severity,desc:currBug.bug.description,link:currBug.bug.link,bug:currBug,assigned:isAssigned,isPMBug:false,isTSTBug:false});
                                    }
                                }
                            }
            }
                    
    }).catch(err => {console.log(err)});
        }
    }
    changeMessage=(value)=>{
         this.setState({msg:value})}
    deleteBug=()=>{
        //  let str=`http://3.16.216.202:8080/api/user/project/bug/deleteBug`;
        //  axios.delete(str,{params:{email:this.state.email,projID:this.state.projID,bugID:this.state.id}}).then((res)=>{
        //      if(res.data){
        //          if(res.data.message){
        //              this.changeMessage(res.data.message);
        //          }//else this.props.onProjectChanged();
        //      }
        //  }).catch(err => {console.log(err)});
    }
    // componentWillUnmount() {
    // this._isMounted = false;
    // }
    componentDidMount=()=>{
        // this._isMounted = true;
        let isAssigned=false;
        let isPMB=false;
        let isTstB=false;
        // if(this.state.bug.solution.solutionID)
        //     isAssigned=true;
        let state=this.state;
        let str='http://3.16.216.202:8080/api/project';
        axios.get(str,{params:{projID:this.state.projID}}).then((res)=>{
            if(res.data){
                if(res.data.message){
                    this.changeMessage(res.data.message);
                    //return -1;
                }else{
                    console.log(res);
                    //console.log()
                    let currBug=null;
                    console.log(this.props.id);
                    console.log(res.data.tsts[0].bugs[0].bug.id);
                    let found=false;
                    for(let i=0;i<res.data.tsts.length;i++){
                        for(let j=0;j<res.data.tsts[i].bugs.length;j++){
                            if(res.data.tsts[i].bugs[j].bug.id===this.props.id)
                                {
                                    console.log('found');
                                    currBug=res.data.tsts[i].bugs[j];
                                    found=true;
                                    break;
                                }}
                        if(found===true){
                            break;
                        }
                    }
                    for(let i=0;i<res.data.bgsNoT.length;i++)
                            if(res.data.bgsNoT[i].id===this.props.id)
                                {
                                    currBug=res.data.bgsNoT[i];
                                    break;
                                }
                    console.log('in bug view');
                    console.log(currBug);
                    if(currBug.solution[0].solutionID)
                                        isAssigned=true;
                   let check=false;
                                for(let i=0;i<res.data.pm.length;i++){
                                    if(res.data.pm[i].userEmail===this.state.email)
                                        check=true;
                                }
                                if(check===true){
                                    if(currBug.solution[0].PM_EMAIL===this.state.email)
                                        this.setState({type:"pm",/*solutionLink:currBug.solution[0].solutionLink,*/PM_EMAIL:currBug.solution[0].PM_EMAIL,severity:currBug.bug.severity,desc:currBug.bug.description,link:currBug.bug.link,bug:currBug,assigned:isAssigned,isPMBug:true,isTSTBug:false});
                                        else this.setState({type:"pm"/*,solutionLink:currBug.solution[0].solutionLink,*/,PM_EMAIL:currBug.solution[0].PM_EMAIL,severity:currBug.bug.severity,desc:currBug.bug.description,link:currBug.bug.link,bug:currBug,assigned:isAssigned,isPMBug:false,isTSTBug:false});
                                }else {
                                    let check2=false;
                                    for(let i=0;i<res.data.tsts.length;i++){
                                        if(res.data.tsts[i].tester.email===this.state.email)
                                                check2=true;
                                    }
                                    console.log(check2);
                                    if(check2===true){
                                        let isTBug=false;
                                            for(let i=0;i<res.data.tsts.length;i++){
                                                for(let j=0;j<res.data.tsts[i].bugs.length;j++)
                                                    if(res.data.tsts[i].bugs[j].bug.id===this.props.id && res.data.tsts[i].tester.email===this.props.email)
                                                        {
                                                            console.log("found tester bugs yes");
                                                            isTBug=true;
                                                            break;
                                                        }
                                            }
                                        this.setState({type:"tst"/*,solutionLink:currBug.solution[0].solutionLink*/,PM_EMAIL:currBug.solution[0].PM_EMAIL,severity:currBug.bug.severity,desc:currBug.bug.description,link:currBug.bug.link,bug:currBug,assigned:isAssigned,isPMBug:false,isTSTBug:isTBug});
                                    }else{
                                        this.setState({type:"user"/*,solutionLink:currBug.solution[0].solutionLink*/,PM_EMAIL:currBug.solution[0].PM_EMAIL,severity:currBug.bug.severity,desc:currBug.bug.description,link:currBug.bug.link,bug:currBug,assigned:isAssigned,isPMBug:false,isTSTBug:false});
                                    }
                                }
                            }
            }
                    
    }).catch(err => {console.log(err)});
    }
    editBug=()=>{
        this.setState({editB:true});
    }
    cancelEditBug=()=>{
        this.setState({editB:false});
    }
    handleChangeDescription=(event)=>{
        this.setState({desc:event.target.value});
    }
    handleChangeLink=(event)=>{
        this.setState({link:event.target.value});
    }
    handleChangeSeverity=(event)=>{
         this.setState({severity:event.target.value});
    }
    
    updateBug=()=>{
        let str=`http://3.16.216.202:8080/api/user/project/bug/editBug`;
        let b=this.state;
        axios.put(str,b).then((res)=>{
             if(res.data){
                 if(res.data.message){
                     this.changeMessage(res.data.message);
                 }else this.setState({editB:false});
             }
         }).catch(err => {console.log(err)});
    }
    
    assignBug=()=>{
        let str=`http://3.16.216.202:8080/api/user/project/bug/assign`;
        let b=this.state;
        axios.post(str,b).then((res)=>{
            console.log('in assigning');
             if(res.data){
                 if(res.data.message){
                     //console.log(res.data.message);
                     this.changeMessage(res.data.message);
                 }else this.setState({assigned:true,isPMBug:true});
             }
         }).catch(err => {console.log(err)});
    }
    deassignBug=()=>{
        let str=`http://3.16.216.202:8080/api/user/project/bug/deassign`;
        //let b=this.state;
        axios.delete(str,{params:{email:this.state.email,projID:this.state.projID,id:this.state.bug.bug.id}}).then((res)=>{
             if(res.data){
                 if(res.data.message){
                     this.changeMessage(res.data.message);
                 }else this.setState({assigned:false});
             }
         }).catch(err => {console.log(err)});
    }
    editSol=()=>{
        this.setState({editS:true});
    }
    cancelEditSol=()=>{
        this.setState({editS:false})
    }
    updateSol=()=>{
        let str=`http://3.16.216.202:8080/api/user/project/bug/addSolution`;
        let s=this.state;
        axios.put(str,s).then((res)=>{
            if(res.data){
                console.log('in solution add');
                 if(res.data.message){
                     this.changeMessage(res.data.message);}
                  else this.setState({editS:false});}
        }).catch(err => {console.log(err)});
    }
    handleChangeSol=(event)=>{
        this.setState({solutionLink:event.target.value});
    }
    render(){
       // console.log(this.state);
        if(this.state.type==="user"){
            if(this.state.solutionLink)
                return(
                    <div>
                    <p>{this.state.id} {this.state.severity} {this.state.link} </p>
                    <p>{this.state.desc}</p>
                    <p>{this.state.bug.solution[0].solutionLink} {this.state.PM_EMAIL}</p>
                    <p>{this.state.msg}</p>
                    </div>
                    );
            else 
             return(
                    <div>
                    <p>{this.state.id} {this.state.severity} {this.state.link}</p>
                    <p>{this.state.desc}</p>
                     <p>{this.state.msg}</p>
                    </div>
                );
        }else if(this.state.type==="tst"){
            //can delete or update the current bug if it is yours
            if(this.state.editB===true)
                {
                    return(
                        <div>
                        <label htmlFor="desc">Description</label>
                        <textarea id="desc" type="text" name="desc" rows="10" cols="50" value={this.state.desc} onChange={this.handleChangeDescription}></textarea>
                        <label htmlFor="link">Link</label>
                        <input type="text" name="link" id="link" value={this.state.link} onChange={this.handleChangeLink}/>
                        <label htmlFor="severity">Severity</label>
                        <select 
                            value={this.state.severity}
                            onChange={this.handleChangeSeverity}>
                            <option>Critical</option>
                            <option>Major</option>
                            <option>Minor</option>
                            <option>Trivial</option>
                        </select>
                        <button type="submit" onClick={this.updateBug}>Update bug</button>
                        <button type="submit" onClick={this.cancelEditBug}>Cancel</button>
                        <p>{this.state.msg}</p>
                        </div>);
            }else{
                if(this.state.assigned)
                    return(
                    <div>
                    <p>{this.state.id} {this.state.severity} {this.state.link}</p>
                    <p>{this.state.desc}</p>
                    <p>{this.state.bug.solution[0].solutionLink} {this.state.PM_EMAIL}</p>
                    <p>{this.state.msg}</p>
                    </div>
                    );
                else if(this.state.isTSTBug===true){
                    return(
                        <div>
                        <p>{this.state.id} {this.state.severity} {this.state.link}</p>
                        <p>{this.state.desc}</p>
                        <button id="20" type="button" onClick={this.editBug}>Edit bug</button>
                        <button id="21" type="button" onClick={this.deleteBug}>Delete bug</button>
                        <p>{this.state.msg}</p>
                        </div>
            );}else return(
                        <div>
                        <p>{this.state.id} {this.state.severity} {this.state.link}</p>
                        <p>{this.state.desc}</p>
                        <p>{this.state.bug.solution[0].solutionLink} {this.state.PM_EMAIL}</p>
                        </div>
                );
            }
        }else{//the user is a pm -> assign, edit/add solution and deassign bug
        if(this.state.editS){
            return(
                        <div>
                        <p>{this.state.id} {this.state.severity} {this.state.link}</p>
                        <p>{this.state.desc}</p>
                        <label htmlFor="solution">Solution</label>
                        <input type="text" name="link" id="link" value={this.state.solutionLink} onChange={this.handleChangeSol}/>
                        <button type="submit" onClick={this.updateSol}>Ok</button>
                        <button type="submit" onClick={this.cancelEditSol}>Cancel</button>
                        <p>{this.state.msg}</p>
                        </div>);
        }else
            if(this.state.assigned===false){
                return(
                        <div>
                        <p>{this.state.id} {this.state.severity} {this.state.link}</p>
                        <p>{this.state.desc}</p>
                        <button id="20" type="button" onClick={this.assignBug}>Assign bug</button>
                         <p>{this.state.msg}</p>
                        </div>)
            }else{
                if(this.state.isPMBug===true){
                    return(
                    <div>
                        <p>{this.state.id} {this.state.severity} {this.state.link}</p>
                        <p>{this.state.desc}</p>
                        <p>{this.state.bug.solution[0].solutionLink} {this.state.PM_EMAIL}</p>
                        <button id="20" type="button" onClick={this.editSol}>Edit solution</button>
                        <button id="21" type="button" onClick={this.deassignBug}>Deassign bug</button>
                         <p>{this.state.msg}</p>
                        </div>)
                }else{
                    return(
                    <div>
                        <p>{this.state.id} {this.state.severity} {this.state.link}</p>
                        <p>{this.state.desc}</p>
                        <p>{this.state.bug.solution[0].solutionLink} {this.state.PM_EMAIL}</p>
                        </div>)
                }
            }
        }
    }
}


class EditProject extends Component{
    constructor(props){
        super(props);
        this.state={email:this.props.email,
            msg: null,
            projectName:'',
            projectDescription:'',
            projectLink:'',
            projectMember1:'',
            projectMember2:'',
            projectMember3:'',
            projectMember4:'',
            projectMember5:'',
            projID:this.props.projID
        }
    }
    componentDidMount=()=>{
        let str='http://3.16.216.202:8080/api/project';
            axios.get(str,{params:{projID:this.state.projID}}).then((res)=>{
                if(res.data){
                    if(res.data.message){
                        this.changeMessage(res.data.message);
                        //return -1;
                    }else{
                        this.setState({projectName:res.data.project.name,projectDescription:res.data.project.description,projectLink:res.data.project.link});
                    }
                }}).catch(err => {console.log(err)});
    }
    
    changeMessage=(value)=>{
         this.setState({msg:value})}
    
    saveEditProject=(value)=>{
        let str='http://3.16.216.202:8080/api/user/project/edit';
        let u=this.state;
        axios.put(str,u).then((res)=>{
            if(res.data){
                if(res.data.message){
                    this.changeMessage(res.data.message);
                }else  this.props.onChange(3,this.state.projID);
            }
        }).catch(err => {console.log(err)});
    }
    handleChangeName=(event)=>{
        this.setState({projectName:event.target.value});
    }
    handleChangeDescription=(event)=>{
        this.setState({projectDescription:event.target.value});
    }
    handleChangeLink=(event)=>{
        this.setState({projectLink:event.target.value});
    }
    handleChangeProj5=(event)=>{
        this.setState({projectMember5:event.target.value});
    }
    handleChangeProj1=(event)=>{
        this.setState({projectMember1:event.target.value});
    }
    handleChangeProj2=(event)=>{
        this.setState({projectMember2:event.target.value});
    }
    handleChangeProj3=(event)=>{
        this.setState({projectMember3:event.target.value});
    }
    handleChangeProj4=(event)=>{
        this.setState({projectMember4:event.target.value});
    }
    
    render(){
        return(
            <div>
                <h1>Edit project</h1>
                <hr/>
                <label htmlFor="newName">Project name</label>
                <input type="text" name="newName" id="newName" value={this.state.projectName} onChange={this.handleChangeName}/>
                
                <label htmlFor="newDescription">Project description</label>
                <textarea id="newDescription" type="text" name="newDescription" rows="10" cols="50" value={this.state.projectDescription} onChange={this.handleChangeDescription}></textarea>
                
                <label htmlFor="newLink">Project link</label>
                <input type="text" name="projectLink" id="newLink" value={this.state.projectLink} onChange={this.handleChangeLink}/>
                
                <label htmlFor="projectMember">Project members</label>
                <fieldset>
                  <legend>Project members:</legend>
                  PM2 <input type="text" name="projectMember1" value={this.state.projectMember1} onChange={this.handleChangeProj1}/><br/>
                  PM3 <input type="text" name="projectMember2" value={this.state.projectMember2} onChange={this.handleChangeProj2}/><br/>
                  PM4 <input type="text" name="projectMember3" value={this.state.projectMember3} onChange={this.handleChangeProj3}/><br/>
                  PM5 <input type="text" name="projectMember4" value={this.state.projectMember4} onChange={this.handleChangeProj4}/><br/>
                  PM6 <input type="text" name="projectMember5" value={this.state.projectMember5} onChange={this.handleChangeProj5}/>
              
                </fieldset>
                
                <button id="10" type="button" onClick={this.saveEditProject}>Save project</button>
                <p>{this.state.msg}</p>
                
                <br/>
                <button id="11" type="button" onClick={() => this.props.onChange(3,this.state.projID)}>Cancel</button>
                <br/>
                
            </div>
        );
    }
}

export default HomePage;
