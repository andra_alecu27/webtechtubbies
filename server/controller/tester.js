const tstService=require('../service/tester2');
const pmService=require('../service/projectMember');
const prjService=require('../service/project');
const userService=require('../service/user');

const createTester=async(req,res,next)=>{
    const userEmail=req.body.email;
    const prjID=req.body.projID;
        //check if project exists 
        try{
            let pExists=await prjService.getByID(prjID);
            if(pExists){
                //check is user exists
                try{
                    let user=await userService.getByEmail(userEmail);
                    if(user){
                        try{
                            //check if user is tester already
                            let existsT=await tstService.getTSTFORPROJ(prjID,userEmail);
                                if(existsT){
                                    res.status(258).json({message:'Already tester'});
                                }else{
                                    //not tester 
                                    //check if project member
                                    let existsP=await pmService.getPMFORPROJ(prjID,userEmail);
                                    if(existsP){
                                        res.status(259).json({message:'Already PM! Can not be tester'});
                                    }else{
                                        //not tester and not pm
                                        //can create tester
                                        try{
                                            let obj={
                                                projectId:prjID,
                                                userEmail:userEmail
                                            }
                                            const result=await tstService.create(obj);
                                            res.status(200).json(result);
                                        }catch(err){
                                            res.status(502).send({message:`${err}`});
                                        }
                                    }
                                }
                            }catch(err){
                                res.status(502).send({message:`${err}`});
                            }
                }else{
                     res.status(258).json({message:'User does not exist'});
                }
            }catch(err){
                 res.status(502).send({message:`${err}`});
            }
        }else{
             res.status(257).json({message:'Project does not exist'});
        }
    }catch(err){
        res.status(502).send({message:`${err}`});
    }
}

const getAllTesters=async(req,res,next)=>{
    try{
        const results=await tstService.getAll();
        res.status(200).json(results);
    }catch(err){
          res.status(500).send({message:`Error:${err}`});
    }
}

const getByProjectId=async(req,res,next)=>{
    try{
        const prjId=req.body.id;
        const userId=req.body.userId;
        const result=await tstService.getByPROJECTID(prjId,userId);
        res.status(200).json(result);
    }catch(err){
         res.status(500).send({message:`Error:${err}`});
    }
}

const getByTstId=async(req,res,next)=>{
    try{
        const id=req.body.id;
        const result=await tstService.getByTSTID(id);
        res.status(200).json(result);
    }catch(err){
         res.status(500).send({message:`Error:${err}`});
    }
}
const getByUserId=async(req,res,next)=>{
     try{
        const userId=req.params.bemail;
        const result=await tstService.getByUSERID(userId);
        res.status(200).json(result);
    }catch(err){
         res.status(500).send({message:`Error:${err}`});
    }
}

const getProjectForATst=async(req,res,next)=>{
    try{
        const userId=req.params.bemail;
        if(userId){
            try{
                const results=await tstService.getProjectForATst(userId);
                if(results)
                    res.status(200).json(results);
                else{
                    res.status(278).json({message:'This user is not a tester for any project'});
                }
            }catch(err){
                res.status(500).send({message:`Error:${err}`});
            }
        }
    }catch(err){
         res.status(500).send({message:`Error:${err}`});
    }
}
const getUserIDProjIDTST=async(req,res,next)=>{
        const userId=req.query.email;
        const projID=req.query.projID;
       
            try{
                //check if the project exists
                let pExists=await prjService.getByID(projID);
                if(pExists){
                    //check is user exists
                    try{
                        let user=await userService.getByEmail(userId);
                        if(user){
                            try{
                                const result=await tstService.getTSTFORPROJ(projID,userId);
                                if(result){
                                    res.status(200).json(result);
                                }else{
                                    res.status(256).json({message:'This user is not a tester for this project'});
                                }  
                            }catch(err){
                                res.status(500).send({message:`Error:${err}`});
                                }
                            }else{
                                res.status(255).json({message:'The user does not exist'});
                            }
                    }catch(err){
                        res.status(500).send({message:`Error:${err}`});
                    }
                }else{
                     res.status(253).json({message:'The project does not exist'});
                }
            }catch(err){
                res.status(500).send({message:`Error:${err}`});
            }
        
    
}
const deleteTester=async(req,res,next)=>{
    const userId=req.params.bemail;
    const projID=req.params.bpid;
    try{
                //check if the project exists
                let pExists=await prjService.getByID(projID);
                if(pExists){
                    //check is user exists
                    try{
                        let user=await userService.getByEmail(userId);
                        if(user){
                            try{
                                const result=await tstService.getTSTFORPROJ(projID,userId);
                                if(result){
                                    //user is tester for this project -> delete tester
                                    let result=await tstService.deleteT(userId,projID);
                                    res.status(251).json(result);
                                }else{
                                    res.status(252).json({message:'This user is not a tester for this project'});
                                }  
                            }catch(err){
                                res.status(500).send({message:`Error:${err}`});
                                }
                            }else{
                                res.status(2100).json({message:'The user does not exist'});
                            }
                    }catch(err){
                        res.status(500).send({message:`Error:${err}`});
                    }
                }else{
                     res.status(250).json({message:'The project does not exist'});
                }
            }catch(err){
                res.status(500).send({message:`Error:${err}`});
            }
}


module.exports={
    createTester,
    getAllTesters,
    getByProjectId,
    getByTstId,
    getByUserId,
    getProjectForATst,
    getUserIDProjIDTST,
    deleteTester
}