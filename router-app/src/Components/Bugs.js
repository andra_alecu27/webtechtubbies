import React, { Component } from 'react';

const axios=require('axios');

class Bugs extends Component{ //for user,tst,pm
    constructor(props){
        super(props);
        this.state={user:true,tst:false,pm:false};
        if(this.props.isPM===true){
            this.setState({user:false,pm:true,tst:false});
        }
         if(this.props.isTST===true){
            this.setState({user:false,pm:false,tst:true});
        }
         if(this.props.isUser===true){
            this.setState({user:true,pm:false,tst:false});
        }
    }
    //all tester bugs followed by solution if exists 
    render(){
        if(this.state.user===true){ //view project as user
            return(
                <div>
                <button type="submit" onClick={this.becomeTester}>Become a tester</button>
                <bugList action={"view"} projID={this.props.projID} user={this.props.email}/>
                </div>
            );
        }else if(this.state.tst===true){ //view project as tester
             return(
                <div>
                <button type="submit" onClick={this.addBug}>Add a bug</button>
                <bugList action={"tst"} projID={this.props.projID} user={this.props.email}/>
                </div>
            );
        }else return( //view project as pm
            <div>
            <bugList action={"pm"} projID={this.props.projID} user={this.props.email}/>
            </div>
        );
    }
    
}

class bugList extends Component{
    constructor(props){
         super(props);
         this.state={action:this.props.action,tstBugs:[],noTSTBugs:[],projID:this.props.projID};
         
    }
    componentDidMount=()=>{
        let str=`http://:8080/api/project/${this.state.projID}`;
        axios.get(str).then(project=>{
            if(project.data.tsts){
                this.setState({tstBugs:project.data.tsts});}
            if(project.data.bgsNoT){
               this.setState({noTSTBugs:project.data.tsts});}
        })
    }
    
    render(){
        if (this.state.action==='view'){ //can only view bugs
            const TSTbgs = this.state.tstBugs.map((tst) => {
                if(tst.bugs.length>0){
                    if(tst.tester===this.props.email){
                        return <Tester info={tst.tester} obj={tst} bugs={tst.bugs} isCurrUser={true}/>
                    }else{
                        return <Tester info={tst.tester} obj={tst} bugs={tst.bugs} isCurrUser={false}/>
                    }
                }
                })
            const notstbgs=this.state.noTSTBugs.map((bug)=>{
                if(bug.solution){
                    return <div>{bug.id} {bug.severity} {bug.link} {bug.solution.solutionLink}</div>
                }else{
                    return <div>{bug.id} {bug.severity} {bug.link}</div>
                }
            })
        }
    }
}

class Tester extends Component{
     constructor(props){
         super(props);
         this.state={isCurrUser:this.props.isCurrUser};
    }
    render(){
        if(this.state.isCurrUser===false){
            return(
                <div>
                <p>{this.props.info}</p>
                </div>
                );
        }
    }
}

export default Bugs;