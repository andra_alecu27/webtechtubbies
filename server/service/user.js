const{sequelize, User,
     Tester,
     ResolvedBugs,
     Project,
     ProjectMember,
     Bug}=require('../models/index');

const user={
    create:async(user)=>{
        try{
            //const result=await User.create({email:user.email},{password:user.password});
            const result=await User.create(user);
            return result;
        }catch(err){
            throw new Error(err.message);
        }
    },
     getAll:async()=>{
        try{
            const results=await User.findAll();
            return results;
        }catch(err){
             throw new Error(err.message);
        }
    },
     editByEmail:async(emailU,newPassword)=>{
        try{
            const result=await User.update({password:newPassword},{where:{email:emailU}});
            return result;
        }catch(err){
             throw new Error(err.message);
        }
    },
     getByEmail:async(emailU)=>{
        try{
            const result=await User.findOne({where:{email:emailU}});
            return result;
        }catch(err){
              throw new Error(err.message);
        }
    },
    getEmailPassword:async(emailU,passwordU)=>{
        try{
            const result=await User.findOne({where:{email:emailU,password:passwordU}});
            return result;
        }catch(err){
              throw new Error(err.message);
        }
    },deleteU:async(emailU)=>{
         try{
            const result=await User.destroy({where:{email:emailU}});
            return result;
        }catch(err){
              throw new Error(err.message);
        }
    }
}
module.exports=user;