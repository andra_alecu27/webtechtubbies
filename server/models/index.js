const configuration=require('./../config/configuration.json');
const Sequelize=require('sequelize');

const DB_NAME=configuration.database.database_name;
const DB_USER=configuration.database.username;
const DB_PASS=configuration.database.password;

const sequelize=new Sequelize(DB_NAME,DB_USER,DB_PASS,{
    dialect:'mysql',
    define : {
		timestamps : false
	}
});


let User=sequelize.define('user',
{
    email:{
        type:Sequelize.STRING,
        allowNull:false,
        primaryKey:true,
        validate:{
            len:[5,35],
            isEmail:true
        }
    },
    password:{
        type:Sequelize.STRING,
        allowNull:false,
        validate:{
            len:[6,20]
        },
        set(val) {
            if(val)
                this.setDataValue('password', val);
        }
    }
});

let Project=sequelize.define('project', 
{
    id:{
        type:Sequelize.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    },
    name:{
        type:Sequelize.TEXT,
        allowNull:false,
        set(val) {
            this.setDataValue('name', val.toLowerCase());
        }
    },
    description:{
        type:Sequelize.TEXT,
        allowNull:false,
    },
    link:{
        type:Sequelize.STRING,
        validate: {
            isUrl:true
        }
    }
});
    
let Tester=sequelize.define('tester',{
    t_id:{
        type:Sequelize.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    }
    
});

let ProjectMember=sequelize.define('projectMember',{
     p_id:{
        type:Sequelize.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    }
});
    
let Bug=sequelize.define('bug',{
     id:{
        type:Sequelize.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    },
    severity:{
        type:Sequelize.STRING,
        allowNull:false,
        set(val) {
            if(val)
                this.setDataValue('severity', val);
        }
    },
    description:{
        type:Sequelize.TEXT,
        allowNull:false,
        set(val) {
            if(val)
                this.setDataValue('description', val);
        }
    },
    link:{
        type:Sequelize.STRING,
        validate: {
            isUrl:true
        }
    }
});

let ResolvedBugs=sequelize.define('resolvedBug',{
     id:{
        type:Sequelize.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    },solution:{
        type:Sequelize.STRING,
        validate: {
            isUrl:true
        }
    }
});


Project.hasMany(ProjectMember, { onDelete: 'cascade' /*, foreignKey: 'id'*/ });
ProjectMember.belongsTo(Project/*, { foreignKey: 'id' }*/);
Project.hasMany(Tester, { onDelete: 'cascade'/*, foreignKey: 'id'*/ });
Tester.belongsTo(Project/*, { foreignKey: 'id' }*/);

User.hasMany(ProjectMember, { onDelete: 'cascade'/*, foreignKey: 'email' */});
ProjectMember.belongsTo(User/*, { foreignKey: 'email' }*/);
User.hasMany(Tester, { onDelete: 'cascade' /*, foreignKey: 'email'*/ });
Tester.belongsTo(User/*, { foreignKey: 'email' }*/);

Tester.hasMany(Bug/*, { onDelete: 'cascade' }*/);
Bug.belongsTo(Tester);
Bug.belongsTo(Project,{onDelete:'cascade'});
ProjectMember.hasMany(ResolvedBugs/*, { onDelete: 'cascade' }*/);
ResolvedBugs.belongsTo(ProjectMember);
Bug.hasOne(ResolvedBugs);
ResolvedBugs.belongsTo(Bug, { onDelete: 'cascade' });


sequelize.sync({force:true}); //->drops table if exists and then creates 
//sequelize.sync({force:false});

module.exports={
    sequelize,
     User,
     Tester,
     ResolvedBugs,
     Project,
     ProjectMember,
     Bug
}

