const userService=require('../service/user');
const pmService=require('../service/projectMember');
const projectService=require('../service/project');

const createUser=async(req,res,next)=>{
    const user=req.body;
    if(user.email && user.pwd && user.repeatPWD){
        if(user.pwd===user.repeatPWD ){
            if(user.pwd.length>5 && user.pwd.length<21){
                let u={
                    email:user.email,
                    password:user.repeatPWD
                }
                try{
                    const users=await userService.getByEmail(u.email);
                    if(users){
                        res.status(214).json({message:`User already exists`});
                    }
                    else{
                         try{
                            const result=await userService.create(u);
                            res.status(201).json(result);
                        }catch(err){
                            res.status(502).send({message:`${err}`});
                        }
                    }
                }catch(err){
                      res.status(502).json({message:`${err}`});
                }
            }else{
                res.status(205).json({message:`Passwords must be between 6 and 20 char`});}
        }else{
            res.status(201).json({message:'Passwords must match'});
        }
    }else{
        res.status(202).json({message:'All fields must be completed'});
    }
}

const getAllUsers=async(req,res,next)=>{
    try{
        const results=await userService.getAll();
        res.status(200).json(results);
    }catch(err){
         res.status(500).send({message:`Error:${err}`});
    }
}

const editByEmail=async(req,res,next)=>{
    try{
        const userEmail=req.body.email;
        const newPassword=req.body.NewPassword;
        if(userEmail&&newPassword){
            if(newPassword.length>5 && newPassword.length<21){
            try{
                const users=await userService.getByEmail(userEmail);
                    if(users){
                        try{
                            const result=await userService.editByEmail(userEmail,newPassword);
                            res.status(220).json(result);
                        }catch(err){
                            res.status(502).send({message:`${err}`});
                        }
                    }
                    else{
                         res.status(222).json({message:'No such user found'});
                    }
            }catch(err){
                res.status(500).send({message:`Error:${err}`});
            }
            }else{
                 res.status(221).json({message:'Password must be between 6 and 20 characters'});
            }
        }else{
                res.status(221).json({message:'All fields must be completed'});
            }
    }catch(err){
         res.status(500).send({message:`Error:${err}`});
    }
}

const getByEmail=async(req,res,next)=>{
    try{
        const userEmail=req.params.bemail;
        if(userEmail){
            try{
                const result=await userService.getByEmail(userEmail)
                if(result)
                    res.status(230).json(result); 
                    else res.status(231).json({message:'No such user'});
            }catch(err){
                res.status(500).send({message:`Error:${err}`});
            }
        }else{
             res.status(232).json({message:'Incorrect params'});
        }
    }catch(err){
         res.status(500).send({message:`Error:${err}`});
    }
}
const getEmailPassword=async(req,res,next)=>{
     try{
        const userEmail=req.query.email;
        const userPassword=req.query.pwd;
        if(userEmail && userPassword){
            try{
                const exists=await userService.getByEmail(userEmail)
                if(exists){
                    try{
                        const result=await userService.getEmailPassword(userEmail,userPassword);
                        if(result){
                            res.status(242).json(result); 
                        }else{
                            res.status(241).json({message:'Wrong password'});
                        }
                    }catch(err){
                         res.status(500).send({message:`Error:${err}`});
                    }
                }else{
                    res.status(243).json({message:'User does not exist'});
                }
            }catch(err){
                res.status(500).json({message:`Error:${err}`});
            }
        }else{
            res.status(243).json({message:'All fields must be completed'});
        }
    }catch(err){
          res.status(500).send({message:`Error:${err}`});
    }
}
const deleteUser=async(req,res,next)=>{
     try{
        const userEmail=req.query.email;
        if(userEmail){
            try{
                const result=await userService.getByEmail(userEmail)
                if(result){
                    try{//delete the projects where the current user is the only pm
                        let prjs=await pmService.getProjectForPM(userEmail);
                        //res.status(200).json(prjs); 
                        let i=0;
                        while(i<prjs.length){
                        //for(let i=0;i<prjs.length;i++){
                            try{
                                let noPMs=await pmService.checkNoPM(prjs[i].id);
                                 //res.status(200).json(noPMs); 
                                 //res.status(200).send({messag:`Id project : ${prjs[i].id}`});
                                if(noPMs.length===1){
                                    //res.status(200).send({messag:`Id project : ${prjs[i].id}`});
                                    try{
                                    //the current user is the only PM
                                        let idProjToDelete=parseInt(prjs[i].id);
                                        prjs.splice(i,1);
                                        let delp=await projectService.deletePJ(idProjToDelete);
                                         //const del=await projectService.deletePJ(id);
                                    }catch(err){
                                        res.status(500).send({message:`Error:${err}`});
                                    }
                                }else i++;
                            }catch(err){
                                res.status(500).send({message:`Error:${err}`});
                            }
                        }
                        let del=await userService.deleteU(userEmail);
                        res.status(200).json(del); 
                    }catch(err){
                        res.status(500).json({message:`Error:${err}`});
                    }
                }
                else res.status(202).send({message:'No such user'});
            }catch(err){
                res.status(500).json({message:`Error:${err}`});
            }
        }else{
             res.status(202).json({message:'Incorrect params'});
        }
    }catch(err){
         res.status(500).send({message:`Error:${err}`});
    }
}

module.exports={
    createUser,getAllUsers,editByEmail,getByEmail,getEmailPassword,deleteUser
}