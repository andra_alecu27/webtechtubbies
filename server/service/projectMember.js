const{sequelize, User,
     Tester,
     ResolvedBugs,
     ProjectMember,
     Project,
     Bug}=require('../models/index');

const pm={
    create:async(mail,prj)=>{
        try{
            const result=await ProjectMember.create({projectId:prj},{userEmail:mail});
            return result;
        }catch(err){
            throw new Error(err.message);
        }
    },
     getAll:async()=>{
        try{
            const results=await ProjectMember.findAll();
            return results;
            
        }catch(err){
             throw new Error(err.message);
        }
    },
    //get project member -> pm id
     getByPMID:async(pmID)=>{
        try{
            const result=await ProjectMember.findAll({where:{p_id:pmID}});
            return result;
        }catch(err){
             throw new Error(err.message);
        }
    },
    //all project members -> user id
    getByUSERID:async(userID)=>{
        try{
            const result= ProjectMember.findAll({where:{userEmail:userID}});
            return result;
        }catch(err){
             throw new Error(err.message);
        }
    },
    //all project members for a specific project -> project id
    getByPROJECTID:async(pID)=>{
        try{
            const result=await ProjectMember.findAll({where:{projectId:pID}});
            return result;
        }catch(err){
             throw new Error(err.message);
        }
    },
    //get PM for a specific project and user -> user id and project id
    getPMFORPROJ:async(projID,userID)=>{
        try{
            const result=await ProjectMember.findOne({where:{projectId:projID,userEmail:userID}});
            return result;
        }catch(err){
             throw new Error(err.message);
        }
    },
    getProjectForPM:async(userEmail)=>{
        try{
            const results=await sequelize.query('SELECT projects.id AS id , projects.name AS name, projects.description AS description, projects.link AS link FROM ((users INNER JOIN projectMembers ON users.email=projectMembers.userEmail) INNER JOIN  projects ON projects.id=projectMembers.projectId) WHERE  users.email=:status',
            {replacements:{status:userEmail},type:sequelize.QueryTypes.SELECT}).then(function(projects){return projects});
            return  results;
        }catch(err){
            throw new Error(err.message);
        }
    },
    deletePMember:async(projID,userID)=>{
        try{
            const result=await ProjectMember.destroy({where:{projectId:projID,userEmail:userID}});
            return result;
        }catch(err){
             throw new Error(err.message);
        }
    },checkNoPM:async(prjID)=>{
         try{
            const results=await sequelize.query('SELECT p_id AS count FROM projectMembers WHERE projectId=:status',
            {replacements:{status:prjID},type:sequelize.QueryTypes.SELECT}).then(function(numberPM){return numberPM});
            return  results;
        }catch(err){
            throw new Error(err.message);
        }
    }
}
module.exports=pm;