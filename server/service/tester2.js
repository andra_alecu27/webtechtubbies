const{sequelize, User,
     Tester,
     ResolvedBugs,
     Project,
     ProjectMember,
     Bug}=require('../models/index');
const tst={
    create:async(tst)=>{
        try{
            const result=await Tester.create(tst);
            return result;
        }catch(err){
            throw new Error(err.message);
        }
    },
     getAll:async()=>{
        try{
            const results=await Tester.findAll();
            return results;
        }catch(err){
             throw new Error(err.message);
        }
    },
    //find tester -> tester id
     getByTSTID:async(tstID)=>{
        try{
            const result=await Tester.findOne({where:{t_id:tstID}});
            return result;
        }catch(err){
             throw new Error(err.message);
        }
    },
    //all testers for a user -> user id
    getByUSERID:async(userID)=>{
        try{
            const result= Tester.findAll({where:{userEmail:userID}});
            return result;
        }catch(err){
             throw new Error(err.message);
        }
    },
    //all the testers for a project -> project id
    getByPROJECTID:async(pID)=>{
        try{
            const result=await Tester.findAll({where:{projectId:pID}});
            return result;
        }catch(err){
             throw new Error(err.message);
        }
    },
    //find tester for project -> project id and user id
    getTSTFORPROJ:async(projID,userID)=>{
        try{
            const result=await Tester.findOne({where:{projectId:projID,userEmail:userID}});
            return result;
        }catch(err){
             throw new Error(err.message);
        }
    },
    //find projects where user is tester -> user id
    getProjectForATst:async(userEmail)=>{
        try{
            const results=await sequelize.query('SELECT projects.id AS id , projects.name AS name, projects.description AS description, projects.link AS link FROM ((users INNER JOIN testers ON users.email=testers.userEmail) INNER JOIN  projects ON projects.id=testers.projectId) WHERE  users.email=:status',{replacements:{status:userEmail},type:sequelize.QueryTypes.SELECT}).then(function(projects){return projects});
            return  results;
        }catch(err){
            throw new Error(err.message);
        }
    },
    deleteT:async(userID,projID)=>{
        try{
            const result=await Tester.destroy({where:{projectId:projID,userEmail:userID}});
            return result;
        }catch(err){
            throw new Error(err.message);
        }
    }
    
}
module.exports=tst;