const projectService=require('../service/project');
const userService=require('../service/user');
const pmService=require('../service/projectMember');
const tstService=require('../service/tester2');

const createProject=async(req,res,next)=>{
    const name=req.body.projectName;
    const description=req.body.projectDescription;
    const link=req.body.projectLink;
    //const noPM=req.body.noProjectMembers;
    let pm0=req.body.email;
    let pms=[];
    let pm1=req.body.projectMember1;
    let pm2=req.body.projectMember2;
    let pm3=req.body.projectMember3;
    let pm4=req.body.projectMember4;
    let pm5=req.body.projectMember5;
    pms.push(pm0);
    if(pm1)
        pms.push(pm1);
    if(pm2)
        pms.push(pm2);
    if(pm3)
        pms.push(pm3);
    if(pm4)
        pms.push(pm4);
    if(pm5)
        pms.push(pm5);
    if(name && description && link && pm0){
        //have to check if the user exists
            try{
                if(pm0){
                    try{
                        const user=await userService.getByEmail(pm0)
                        if(user){
                            //check if the project members exist as users
                            let pmExists=true;
                            for(let i=0;i<pms.length;i++){
                                    try{
                                        let pmEx=await userService.getByEmail(pms[i]);
                                        if(!pmEx)
                                            pmExists=false;
                                    }catch(err){
                                        res.status(500).send({message:`Error:${err}`});
                                    }
                                }
                            if(pmExists==true){
                                //all project members are users 
                                //create project + add rows in projectMembers table
                                const result=await projectService.create(name,description,link,pms);
                                res.status(200).json(result);
                            }else{
                                 res.status(202).send({message:'Project member is not user'});
                            }
                        }else{
                             res.status(202).json({message:'User does not exist'});
                        }
                    }catch(err){
                        res.status(500).send({message:`Error:${err}`});
                    }
                }else{
                     res.status(202).json({message:'error'});
                }
            }catch(err){
                 res.status(500).send({message:`Error:${err}`});
            }
    }else{
        res.status(206).json({message:'All fields must be completed'});
    }
}

const getAllProjects=async(req,res,next)=>{
    try{
        const results=await projectService.getAll();
        res.status(200).json(results);
    }catch(err){
        res.status(500).send({message:`Error:${err}`});
    }
}

const editByID=async(req,res,next)=>{
    try{
        let pm0=req.body.email;
        const projectID=req.body.projID; //id-ul proiectului pe care-l editam
        const name=req.body.projectName;
        const description=req.body.projectDescription;
        const link=req.body.projectLink;
       // let pm0=req.params.bemail;
        let pms=[];
        let pm1=req.body.projectMember1;
        let pm2=req.body.projectMember2;
        let pm3=req.body.projectMember3;
        let pm4=req.body.projectMember4;
        let pm5=req.body.projectMember5;
        pms.push(pm0);
        if(pm1)
            pms.push(pm1);
        if(pm2)
            pms.push(pm2);
        if(pm3)
            pms.push(pm3);
        if(pm4)
            pms.push(pm4);
        if(pm5)
            pms.push(pm5);
        if(name&&description&&link){
            //check if project exists
            try{
                let project=await projectService.getByID(projectID);
                if(project){
                    //check if the one editing is a user
                    try{
                        let user=await userService.getByEmail(pm0);
                        if(user){
                            //check if the one editing is a project member for this project
                            try{
                            let userPm=await pmService.getPMFORPROJ(projectID,pm0);
                            if(userPm){
                                //check if all project members are users and not testers
                                 let pmExists=true;
                                    for(let i=0;i<pms.length;i++){
                                            try{
                                                let pmEx=await userService.getByEmail(pms[i]);
                                                if(!pmEx)
                                                    pmExists=false;
                                            }catch(err){
                                                res.status(500).send({message:`Error:${err}`});
                                            }
                                        }
                                    if(pmExists==true){
                                        //all project members are users 
                                        //check if all project members are not testers for this project
                                        let tstExists=false;
                                        for(let i=0;i<pms.length;i++){
                                            try{
                                                let abc=await tstService.getTSTFORPROJ(projectID,pms[i]);
                                                if(abc){
                                                    tstExists=true;
                                                }
                                            }catch(err){
                                                 res.status(500).send({message:`Error:${err}`});
                                            }
                                        }
                                        if(tstExists==true){
                                            res.status(272).json({message:'User is a tester for this project'});
                                        }else{
                                            //update the project 
                                            try{
                                                const result=await projectService.editByID(projectID,name,description,link,pms);
                                                res.status(200).json(result);
                                            }catch(err){
                                                res.status(500).send({message:`Error:${err}`});
                                            }
                                        }
                                    }else{
                                        res.status(272).json({message:'User does not exist'});
                                    }
                            }else{
                                res.status(274).json({message:'User is not a project member'});
                            }
                        }catch(err){
                            res.status(500).send({message:`Error:${err}`});
                        }
                        }else{
                            res.status(276).json({message:'User does not exist'});
                        }
                    }catch(err){
                        res.status(500).send({message:`Error:${err}`});
                    }
                }else{
                    res.status(2790).json({message:'Project does not exist'});
                }
            }catch(err){
                res.status(500).send({message:`Error:${err}`});
            }
        }else{
            res.status(278).json({message:'All fields must be completed'});
        }
    }catch(err){
        res.status(500).send({message:`Error:${err}`});
    }
}

const getByID=async(req,res,next)=>{
    try{
        const id=req.query.projID;
        try{
            const result=await projectService.getByID(id);
            if(result)
                res.status(200).json(result);
            else{
                 res.status(270).json({message:'Project does not exist'});
            }
        }catch(err){
            res.status(500).send({message:`Error:${err}`});
        }
    }catch(err){
         res.status(500).send({message:`Error:${err}`});
    }
}

const getByName=async(req,res,next)=>{
    try{
        const nameS=req.body.name;
        //if(nameS){
            try{
                const results=await projectService.getByName(nameS);
                if(results.length>0){
                    res.status(200).json(results);
                }else{
                    res.status(280).json({message:'No projescts'});
                }
            }catch(err){
               res.status(500).send({message:`Error:${err}`}); 
            }    
        // }else{
        //     try{
        //         res.status(2100).send({message:'Project does not exist2'});
        //     }catch(err){
        //         res.status(500).send({message:`Error:${err}`});
        //     }
        // }
    }catch(err){
        res.status(500).send({message:`Error:${err}`});
    }
}

const deleteProject=async(req,res,next)=>{
    try{
        const id=req.query.projID;
        const email=req.query.email;
        try{
            const result=await projectService.getByID(id);
            if(result){
                try{
                        let user=await userService.getByEmail(email);
                        if(user){
                            //check if the one editing is a project member for this project
                            try{
                            let userPm=await pmService.getPMFORPROJ(id,email);
                            if(userPm){
                                //remove project
                                try{
                                    const del=await projectService.deletePJ(id);
                                    res.status(200).json(result);
                                }catch(err){
                                     res.status(500).send({message:`Error:${err}`});
                                }
                            }else{
                                res.status(275).json({message:'User is not a project member'});
                            }
                        }catch(err){
                            res.status(500).send({message:`Error:${err}`});
                        }
                        }else{
                            res.status(274).json({message:'User does not exist'});
                        }
                    }catch(err){
                        res.status(500).send({message:`Error:${err}`});
                    }
                }
            else{
                 res.status(273).json({message:'Project does not exist'});
            }
        }catch(err){
            res.status(500).send({message:`Error:${err}`});
        }
    }catch(err){
         res.status(500).send({message:`Error:${err}`});
    }
}

module.exports={
    createProject,getAllProjects,editByID,getByID,getByName,deleteProject
}