const{sequelize, User,
     Tester,
     ResolvedBugs,
     Project,
     ProjectMember,
     Bug}=require('../models/index');

const resolvedBug={
    create:async(rb)=>{
        try{
            //const result=await User.create({email:user.email},{password:user.password});
            const result=await ResolvedBugs.create(rb);
            return result;
        }catch(err){
            throw new Error(err.message);
        }
    },
    checkIfBugIsAssigned:async(bid)=>{
        try{
            const result=await ResolvedBugs.findOne({where:{bugId:bid}});
            return result;
        }catch(err){
            throw new Error(err.message);
        }
    },
     checkIfBugIsAssignedToPM:async(bid,pmID)=>{
        try{
            const results=await ResolvedBugs.findOne({where:{bugId:bid,projectMemberPId:pmID}});
            return results;
        }catch(err){
             throw new Error(err.message);
        }
    },
     editByRBugID:async(sol,rbID)=>{
        try{
            const result=await ResolvedBugs.update({solution:sol},{where:{id:rbID}});
            return result;
        }catch(err){
             throw new Error(err.message);
        }
    },
     deleteRB:async(rbID)=>{
        try{
            const result=await ResolvedBugs.destroy({where:{id:rbID}});
            return result;
        }catch(err){
              throw new Error(err.message);
        }
    }
}
module.exports=resolvedBug;