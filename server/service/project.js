const{sequelize,
     User,
     Tester,
     ResolvedBugs,
     Project,
     ProjectMember,
     Bug}=require('../models/index');
const Sequelize=require('sequelize');
const Op = Sequelize.Op;
const project={
    //array of user emails
    create:async(Name,Description,Link,listPM)=>{
        try{
            const result=await Project.create({name:Name,link:Link,description:Description});
            let pms=[];
            for(let i=0;i<listPM.length;i++){
                pms.push(await ProjectMember.create({projectId:result.id,userEmail:listPM[i]}));
            }
            const obj={
                project:result,
                pm:pms
            };
            //return project, list of pm
            return obj;
        }catch(err){
            throw new Error(err.message);
        }
    },
     getAll:async()=>{
        try{
            const results=await Project.findAll();
            return results;
            
        }catch(err){
             throw new Error(err.message);
        }
    },//listPM - array of user emails;
     editByID:async(ID, newName, newDescription, newLink, listPM/*,listBugs*/)=>{
        try{
            await Project.update({name:newName,link:newLink,description:newDescription},{where:{id:ID}});
            //await ProjectMember.destroy({where:{projectId:ID}})
            let pms=[];
            
            let currentPM=await ProjectMember.findAll({where:{projectId:ID}})
            
            for(let i=0;i<currentPM.length;i++){
                let exists=false;
                for(let j=0;j<listPM.length;j++){
                    if(currentPM[i].userEmail===listPM[j]){
                        exists=true;
                    }
                }
                if(exists==false){
                     await ProjectMember.destroy({where:{projectId:ID,userEmail:currentPM[i]}});
                }
                if(exists==true){
                     pms.push(currentPM[i]);
                }
            }
            
            currentPM=await ProjectMember.findAll({where:{projectId:ID}})
            
            for(let i=0;i<listPM.length;i++){
                let exists=false;
                for(let j=0;j<currentPM.length;j++){
                    if(currentPM[j].userEmail===listPM[i]){
                        exists=true;
                    }
                }
                if(exists==false){
                    await ProjectMember.create({projectId:ID,userEmail:listPM[i]});
                    //pms.push(JSON.parse(JSON.stringify(pmember)));
                    //pms.push();
                    pms.push(await ProjectMember.findOne({where:{projectId:ID,userEmail:listPM[i]}}));
                }
            }
            let listTesters=Tester.findAll({where:{projectId:ID}})
            let bugs=[];
            // for(let i=0;i<listTesters.length;i++){
            //     let bgs=await Bug.findAll({where:{testerTId:listTesters[i].t_id}});
            //     for(let j=0;j<bgs.length;j++){
            //         bugs.push(bgs[j]);
            //     }
            // }
           //object with testers; each tester has a list of bugs which have solution link and solution id if they have been solved
                let testers=[];
                let testerEmails= await sequelize.query('SELECT users.email AS email,testers.t_id AS tstID FROM users INNER JOIN testers ON users.email=testers.userEmail WHERE testers.projectID= :pID',{replacements:{pID:ID},type:sequelize.QueryTypes.SELECT}).then(function(emails){return emails;})
                for(let i=0;i<testerEmails.length;i++){
                    let bs=await Bug.findAll({where:{testerTId:testerEmails[i].tstID}});
                    if(bs)
                        bugs.push(bs);
                    let rbs=[];
                    //bs.length=0 even if bugs exist
                    //if exists resolved bug for each bug
                    for(let j=0;j<bs.length;j++){
                        let sol=await sequelize.query('SELECT resolvedBugs.id AS solutionID, resolvedBugs.solution AS solutionLink, resolvedBugs.projectMemberPId AS PM FROM bugs LEFT JOIN resolvedBugs ON bugs.id=resolvedBugs.bugId WHERE bugs.id=:BID',{replacements:{BID:bs[i].id},type:sequelize.QueryTypes.SELECT}).then(function(bugs){return bugs;})   
                        rbs.push({bug:bs[i],solution:JSON.parse(JSON.stringify(sol))});
                    }
                    testers.push({tester:testerEmails[i], bugs:JSON.parse(JSON.stringify(rbs))});
                }
                let bgsNoTester=await Bug.findAll({where:{projectId:ID,testerTId:null}});
                let bgsFinalNoTester=[];
                //let bgsNoTester=[];
                if(bgsNoTester.length>0){
                    //bgsNoTester.push(bugsWithNoTester);
                    let rbs=[];
                    for(let i=0;i<bgsNoTester.length;i++){
                        let sol=await sequelize.query('SELECT resolvedBugs.id AS solutionID, resolvedBugs.solution AS solutionLink, resolvedBugs.projectMemberPId AS PM FROM bugs LEFT JOIN resolvedBugs ON bugs.id=resolvedBugs.bugId WHERE bugs.id=:BID',{replacements:{BID:bgsNoTester[i].id},type:sequelize.QueryTypes.SELECT}).then(function(bugs){return bugs;})   
                        bgsFinalNoTester.push({bug:bgsNoTester[i],solution:JSON.parse(JSON.stringify(sol))});
                    }
                }
            let resultP=await Project.findOne({where:{id:ID}});
            let obj={
                project:resultP, //project
                pm:pms, //list of project members
                lbugs:bgsFinalNoTester, //all bugs with no testers;
                tsts:testers //a list of all tester+bugs combo for that project
            };
            return obj;
        }catch(err){
             throw new Error(err.message);
        }
    },
     getByName:async(Name)=>{
        try{
            let results;
            if(Name){
                let value=Name.toLowerCase();
                results=await Project.findAll({
                            where: {
                                    name: {
                                      [Op.like]: `%${value}%`
                                    }
                                  }
                                });
                return results;
            }
            else{
                //results=await Project.findAll({order:['name','ASC']});
                results=await Project.findAll({order: sequelize.literal('name ASC')});
                return results;
            }
        }catch(err){
             throw new Error(err.message);
        }
    },
    getByID:async(ID)=>{
        try{
            //let results;
            let resultP=await Project.findOne({where:{id:ID}});
            if(resultP)
            {
                //all emails that are members of this project 
                //let pmEmails=await sequelize.query('SELECT users.email AS email, projectMembers.p_id AS pmID FROM users INNER JOIN projectMembers ON users.email=projectMembers.userEmail WHERE projectMembers.projectID= :pID',{replacements:{pID:ID},type:sequelize.QueryTypes.SELECT}).then(function(emails){return emails;})    
                //all emails that are testers of this project
                let pmEmails=await ProjectMember.findAll({where:{projectId:ID}});
                // let pms=[];
                // for(let i=0;i<pmEmails.length;i++){
                //     pms.push({projectId:ID,userEmail:pmEmails[i].user,pmID:pmEmails[i].pmID});
                // }
                
                //object with testers; each tester has a list of bugs which have solution link and solution id if they have been solved
                let testers=[];
                let testerEmails= await sequelize.query('SELECT users.email AS email,testers.t_id AS tstID FROM users INNER JOIN testers ON users.email=testers.userEmail WHERE testers.projectID= :pID',{replacements:{pID:ID},type:sequelize.QueryTypes.SELECT}).then(function(emails){return emails;})
                for(let i=0;i<testerEmails.length;i++){
                    let bs=await Bug.findAll({where:{testerTId:testerEmails[i].tstID}});
                    let rbs=[];
                    //bs.length=0 even if bugs exist
                    //if exists resolved bug for each bug
                    for(let j=0;j<bs.length;j++){
                        let sol=await sequelize.query('SELECT resolvedBugs.id AS solutionID, resolvedBugs.solution AS solutionLink, resolvedBugs.projectMemberPId AS PM, projectMembers.userEmail AS PM_EMAIL FROM ((bugs LEFT JOIN resolvedBugs ON bugs.id=resolvedBugs.bugId) LEFT JOIN projectMembers on projectMembers.p_id=resolvedBugs.projectMemberPId) WHERE bugs.id=:bID',{replacements:{bID:bs[j].id},type:sequelize.QueryTypes.SELECT}).then(function(bugs){return bugs;})   
                        //rbs.push({bug:JSON.parse(JSON.stringify(bs[i])),solution:JSON.parse(JSON.stringify(sol))});
                        rbs.push({bug:bs[j],solution:sol});
                    }
                     testers.push({tester:testerEmails[i], bugs:rbs});
                    //testers.push({tester:testerEmails[i], bugs:JSON.parse(JSON.stringify(rbs))});
                }
                let bgsNoTester=await Bug.findAll({where:{projectId:ID,testerTId:null}});
                let bgsFinalNoTester=[];
                //let bgsNoTester=[];
                if(bgsNoTester.length>0){
                   // bgsNoTester.push(bugsWithNoTester);
                    let rbs=[];
                    for(let i=0;i<bgsNoTester.length;i++){
                        let sol=await sequelize.query('SELECT resolvedBugs.id AS solutionID, resolvedBugs.solution AS solutionLink, resolvedBugs.projectMemberPId AS PM, projectMembers.userEmail AS PM_EMAIL FROM ((bugs LEFT JOIN resolvedBugs ON bugs.id=resolvedBugs.bugId) LEFT JOIN projectMembers ON projectMembers.p_id=resolvedBugs.projectMemberPId) WHERE bugs.id=:BID',{replacements:{BID:bgsNoTester[i].id},type:sequelize.QueryTypes.SELECT}).then(function(bugs){return bugs;})   
                        bgsFinalNoTester.push({bug:bgsNoTester[i],solution:JSON.parse(JSON.stringify(sol))});
                    }
                }
                const obj={
                    project:resultP, //project
                    pm:pmEmails, //list of project members
                    tsts:testers,//a list of all tester+bugs+solution combo for that project
                    bgsNoT:bgsFinalNoTester //list of busg with no testers
                };
                return obj;
            }else{
                return null;
            }
        }catch(err){
             throw new Error(err.message);
        }
    },
    deletePJ:async(projID)=>{
        try{
            const results=await Project.destroy({where:{id:projID}});
            return results;
            
        }catch(err){
             throw new Error(err.message);
        }
    }
}
module.exports=project;
