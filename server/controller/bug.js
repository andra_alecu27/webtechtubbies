const projectService=require('../service/project');
const userService=require('../service/user');
const pmService=require('../service/projectMember');
const tstService=require('../service/tester2');
const bugService=require('../service/bug');
const rbService=require('../service/resolvedBug');

const createBug=async(req,res,next)=>{
    const u=req.body;
    const bug=req.body;
    if(bug.severity && bug.desc && bug.link){
        try{
            const user=await userService.getByEmail(u.email);
            if(user){
                try{
                    const proj=await projectService.getByID(u.projID);
                    if(proj){
                        try{
                            const result=await tstService.getTSTFORPROJ(u.projID,u.email);
                                if(result){
                                    try{
                                         let b={
                                            severity:bug.severity,
                                            description:bug.desc,
                                            link:bug.link,
                                            testerTId:result.t_id,
                                            projectId:parseInt(u.projID)
                                            };
                                        const bResult=await bugService.create(b);
                                        res.status(200).json(bResult);
                                    }catch(err){
                                        res.status(500).send({message:`Error:${err}`});
                                    }
                                }else{
                                    res.status(210).json({message:'User is not tester'});}
                        }catch(err){
                             res.status(502).send({message:`${err}`});
                        }
                    }else{
                        res.status(210).json({message:'Project does not exist'});}
                }catch(err){
                    res.status(502).send({message:`${err}`});
                }
            }else{
                res.status(210).json({message:'User does not exist'});}
        }catch(err){
            res.status(502).send({message:`${err}`});
        }
    }else{
        res.status(210).json({message:'All fields must be completed'});
    }
}

const getBugTSTPrj=async(req,res,next)=>{
    const u=req.query;
    //const bug=req.body;
        try{
            const user=await userService.getByEmail(u.email);
            if(user){
                try{
                    const proj=await projectService.getByID(u.projID);
                    if(proj){
                        try{
                            const result=await tstService.getTSTFORPROJ(u.projID,u.email);
                                if(result){
                                    try{
                                        const bResult=await bugService.getBugsForAUserForAProject(u.email,u.projID);
                                        res.status(200).json(bResult);
                                    }catch(err){
                                        res.status(500).send({message:`Error:${err}`});
                                    }
                                }else{
                                    res.status(210).json({message:'User is not tester'});}
                        }catch(err){
                             res.status(502).send({message:`${err}`});
                        }
                    }else{
                        res.status(210).json({message:'Project does not exist'});}
                }catch(err){
                    res.status(502).send({message:`${err}`});
                }
            }else{
                res.status(210).json({message:'User does not exist'});}
        }catch(err){
            res.status(502).send({message:`${err}`});
        }
}

const getBugPrj=async(req,res,next)=>{
    const u=req.query;
    //const bug=req.body;
                try{
                    const proj=await projectService.getByID(u.projID);
                    if(proj){
                        try{
                            const bResult=await bugService.getAllBugsForAProj(u.projID);
                            res.status(200).json(bResult);
                        }catch(err){
                            res.status(500).send({message:`Error:${err}`});
                        }
                    }else{
                        res.status(210).json({message:'Project does not exist'});}
                }catch(err){
                    res.status(502).send({message:`${err}`});
                }
}

const editBug=async(req,res,next)=>{
    const u=req.body;
    const bug=req.body;
    if(bug.severity && bug.desc && bug.link){
        let b={
            severity:bug.severity,
            description:bug.desc,
            link:bug.link
        };
        try{
            const user=await userService.getByEmail(u.email);
            if(user){
                try{
                    const proj=await projectService.getByID(u.projID);
                    if(proj){
                        try{
                            const result=await tstService.getTSTFORPROJ(u.projID,u.email);
                                if(result){
                                    try{
                                        const existsB=await bugService.getByBugID(u.id);
                                        if(existsB){
                                            try{
                                                const check=await bugService.checkifBugIsTSTBug(result.t_id,existsB.id);
                                                if(check){
                                                     try{
                                                        const bResult=await bugService.editBug(b.severity,b.description,b.link,existsB.id);
                                                        res.status(200).json(bResult);
                                                    }catch(err){
                                                        res.status(500).send({message:`Error:${err}`});
                                                    }
                                                }else{
                                                    res.status(210).json({message:'This user does not have access to the bug'});
                                                }
                                            }catch(err){
                                                res.status(210).json({message:'No such bug'});
                                            }
                                        }else{
                                             res.status(210).send({message:'No such bug'});}
                                    }catch(err){
                                        res.status(500).send({message:`Error:${err}`});
                                    }
                                }else{
                                    res.status(210).json({message:'User is not tester'});}
                        }catch(err){
                             res.status(502).send({message:`${err}`});
                        }
                    }else{
                        res.status(210).json({message:'Project does not exist'});}
                }catch(err){
                    res.status(502).send({message:`${err}`});
                }
            }else{
                res.status(210).json({message:'User does not exist'});}
        }catch(err){
            res.status(502).send({message:`${err}`});
        }
    }else{
        res.status(210).json({message:'All fields must be completed'});
    }
}
const deleteBug=async(req,res,next)=>{
    const u=req.query;
     try{
         //check if user exists
            const user=await userService.getByEmail(u.email);
            if(user){
                try{
                    //check if project exists
                    const proj=await projectService.getByID(u.projID);
                    if(proj){
                        try{
                            //check if the user is a tester for this project
                            const result=await tstService.getTSTFORPROJ(u.projID,u.email);
                                if(result){
                                    //check if bug exists
                                    try{
                                        const existsB=await bugService.getByBugID(u.bugID);
                                        if(existsB){
                                            //check if bug is tester's bug
                                            const check=await bugService.checkifBugIsTSTBug(result.t_id,existsB.id);
                                                if(check){
                                                     try{
                                                         //check if bug has been assigned
                                                        const checkAssign=await rbService.checkIfBugIsAssigned(existsB.id);
                                                        if(checkAssign){
                                                             res.status(210).json({message:'Cannot delete bug because it has been assigned'});
                                                        }else{
                                                            //delete bug
                                                            const deleteB=await bugService.deleteBug(existsB.id);
                                                            res.status(200).json(deleteB);
                                                        }
                                                    }catch(err){
                                                        res.status(500).send({message:`Error:${err}`});
                                                    }
                                                }else{
                                                    res.status(210).json({message:'This user does not have access to the bug'});
                                                }
                                        }else{
                                             res.status(210).json({message:'Bug does not exist'});
                                        }
                                    }catch(err){
                                        res.status(502).send({message:`${err}`});
                                    }
                                }else{
                                    res.status(210).json({message:'User is not tester'});}
                        }catch(err){
                             res.status(502).send({message:`${err}`});
                        }
                    }else{
                        res.status(210).json({message:'Project does not exist'});}
                }catch(err){
                    res.status(502).send({message:`${err}`});
                }
            }else{
                res.status(210).json({message:'User does not exist'});}
        }catch(err){
            res.status(502).send({message:`${err}`});
        }
}

module.exports={
    createBug,editBug,getBugTSTPrj,getBugPrj,deleteBug
}