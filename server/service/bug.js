const{sequelize, User,
     Tester,
     ResolvedBugs,
     Project,
     ProjectMember,
     Bug}=require('../models/index');

const bug={
    create:async(bug)=>{ //done
        try{
            const result=await Bug.create(bug);
            return result;
        }catch(err){
            throw new Error(err.message);
        }
    },
    getAllBugsForAProj:async(projID)=>{ 
        try{
            const results=await sequelize.query('SELECT bugs.id AS id, bugs.severity as severity, bugs.description as description, bugs.link as link, testers.t_id as tester_id FROM testers INNER JOIN bugs ON bugs.testerTId=testers.t_id WHERE  testers.projectId=:status',
            {replacements:{status:projID},type:sequelize.QueryTypes.SELECT}).then(function(bugs){return bugs});
            return results;
        }catch(err){
             throw new Error(err.message);
        }
    },
    getBugForATST:async(ID)=>{ 
        try{
            const results=await sequelize.query('SELECT bugs.id AS id, bugs.severity as severity, bugs.description as description, bugs.link as link FROM bugs INNER JOIN testers ON bugs.testerTId=testers.t_id WHERE testers.t_id= :tID',{replacements:{tID:ID},type:sequelize.QueryTypes.SELECT}).then(function(bugs){return bugs;})
            return results;
        }catch(err){
             throw new Error(err.message);
        }
    },
    checkifBugIsTSTBug:async(t_ID,b_ID)=>{ 
        try{
            const results=await sequelize.query('SELECT bugs.id AS id, bugs.severity as severity, bugs.description as description, bugs.link as link FROM bugs INNER JOIN testers ON bugs.testerTId=testers.t_id WHERE testers.t_id= :tID AND bugs.id=:bid',
            {replacements:{tID:t_ID,bid:b_ID},type:sequelize.QueryTypes.SELECT}).then(function(bugs){return bugs;})    
            return results;
        }catch(err){
             throw new Error(err.message);
        }
    },
    getAllBugsForAUser:async(userEmail)=>{ 
        try{
            const results=await sequelize.query('SELECT bugs.id AS id, bugs.severity as severity, bugs.description as description, bugs.link as link, testers.t_id as tester_id, bugs.projectId as project  FROM ((users INNER JOIN testers ON users.email=testers.userEmail) INNER JOIN  bugs ON bugs.testerTId=testers.t_id) WHERE  users.email=:status',
            {replacements:{status:userEmail},type:sequelize.QueryTypes.SELECT}).then(function(bugs){return bugs});
            return results;
        }catch(err){
             throw new Error(err.message);
        }
    },
     editBug:async(s,d,l,bid)=>{ 
        try{
            const result=await Bug.update({severity:s,description:d,link:l},{where:{id:bid}});
            //const result=await sequelize.query('')
            return result;
        }catch(err){
             throw new Error(err.message);
        }
    },
     getByBugID:async(bid)=>{ //done
        try{
            const result=await Bug.findOne({where:{id:bid}});
            return result;
        }catch(err){
              throw new Error(err.message);
        }
    },
    deleteBug:async(bid)=>{ //done
        try{
            const result=await Bug.destroy({where:{id:bid}}); //number of rows deleted
            return result;
        }catch(err){
              throw new Error(err.message);
        }
    },
    getBugsForAUserForAProject:async(userEmail,projID)=>{
        try{
            let bugs= await sequelize.query('SELECT bugs.id AS id, bugs.severity as severity, bugs.description as description, bugs.link as link, testers.userEmail as email FROM ((users INNER JOIN testers ON users.email=testers.userEmail) INNER JOIN  bugs ON bugs.testerTId=testers.t_id) WHERE  users.email=:status1 AND testers.projectId=:status2',{replacements:{status1:userEmail,status2:projID},type:sequelize.QueryTypes.SELECT}).then(function(emails){return emails;})
            return bugs;
        }catch(err){
            throw new Error(err.message)
        }
    }
}
module.exports=bug;