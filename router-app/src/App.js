import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import HomePage from '../src/Components/secondTry'
import SignIn from '../src/Components/SignIn'
//main view
class App extends Component {
constructor(props)
{
  super(props);
  this.state={loggedIn:false,
    userEmail:'',
    password:''
  };
  this.changedLogIn=this.changedLogIn.bind(this);
  this.changedLogOut=this.changedLogOut.bind(this);
}
changedLogIn(mail,pwd){
  this.setState({userEmail:mail,
    loggedIn:true,password:pwd
  })
}
changedLogOut(){
  this.setState({loggedIn:false})
}
  render() {
    if(this.state.loggedIn===true){
      return <HomePage email={this.state.userEmail} onChange={this.changedLogOut} pwd={this.state.pwd}/>;}
      else{
        return (<SignIn onChange={this.changedLogIn}/>);}
  }
}

export default App;
