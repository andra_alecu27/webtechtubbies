import React, { Component } from 'react';

const axios=require('axios');
class SignIn extends Component{
    constructor(props){
        super(props);
        this.state={email:'',
        pwd:'',
        repeatPWD:'',
        loggedIn:false,
        button:1, 
        msg: null}
    }
    changeButton=(value)=>{
        this.setState({button:value});
        this.changeMessage(' ');
    }
    changeMessage=(value)=>{
         this.setState({msg:value})
    }
    ifCredentialsOk=()=>{
        let u=this.state;
        console.log(u);
        axios.get('http://3.16.216.202:8080/api/logIn',{params:{email:this.state.email,pwd:this.state.pwd}}).then(user=>{
                console.log(user);
                if(user.data){
                if(user.data.message)
                   this.changeMessage(user.data.message);
                   else if(user.data.email) this.props.onChange(user.data.email,user.data.password);}
        }).catch(err=>{console.log(err)});
    }
    handleChangeEmail=(event)=>{
        this.setState({email:event.target.value})
    }
    handleChangePWD=(event)=>{
        this.setState({pwd:event.target.value})
    }
    handleChangeRepeatPWD=(event)=>{
        this.setState({repeatPWD:event.target.value})
    }
    createNewAcc=()=>{
        let u=this.state
        console.log(u);
        axios.post('http://3.16.216.202:8080/api/register',u).then(res=>{
            console.log(res);
            if(res.data){
                if(res.data.message)
                    {this.changeMessage(res.data.message);}
                else {
                    if(res.data.email)
                        {this.props.onChange(res.data.email,res.data.password);}
                    }
            }
        }).catch(err=>{console.log(err)});
    }
    
    render(){
        if(this.state.button===1 /*&& this.state.msg===null*/){
            //pag de log in
            return (
                <div>
                <button id="1" type="button" onClick={() => this.changeButton(1)}>Log In</button>
                <button id="2" type="button" onClick={() => this.changeButton(2)}>Create New Acoount</button>
                <br/>
                <label htmlFor="email">Mail</label>
                <input type="text" name="email" id="email" value={this.state.email} onChange={this.handleChangeEmail}/>
                <label htmlFor="pwd">Password</label>
                <input type="text" name="pwd" id="pwd" value={this.state.pwd} onChange={this.handleChangePWD} />
                <button type="submit" onClick={this.ifCredentialsOk}>Ok!</button>
                <p>{this.state.msg}</p>
                </div>
                )
            }
            else{
            if(this.state.button===2 /*&& this.state.msg===null*/){
            //pag inregistr.
            return (
                <div>
                <button id="1" type="button" onClick={() => this.changeButton(1)}>Log In
                </button>
                <button id="2" type="button" onClick={() => this.changeButton(2)}>Create New Account
                </button>
                <br/>
                <label htmlFor="email">Mail</label>
                <input type="text" name="email" id="email" value={this.state.email} onChange={this.handleChangeEmail}/>
                <label htmlFor="pwd">Password</label>
                <input type="text" name="pwd" id="pwd" value={this.state.pwd} onChange={this.handleChangePWD}/>
                <label htmlFor="repeatPWD">Repeat Password</label>
                <input type="text" name="repeatPWD" id="repeatPWD" value={this.state.repeatPWD} onChange={this.handleChangeRepeatPWD}/>
                <button type="submit" onClick={this.createNewAcc}>Ok!</button>
                <p>{this.state.msg}</p>
                </div>
                )}
            }
        }
    }
    
export default SignIn;